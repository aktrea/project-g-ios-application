//
//  ParticipantViewController.m
//  Project-G
//
//  Created by Sabari on 1/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ParticipantViewController.h"
#import "EventViewController.h"
#import "SCLAlertView.h"
#import "DGActivityIndicatorView.h"
#import "Urlclass.h"
@interface ParticipantViewController ()
{
    EventViewController *evn;
    NSString *ipaddress;
    NSDictionary *dictResponse,*dictresponse3;
    NSArray *searchResults,*dictResponse1,*search;
    int tablealert;
    DGActivityIndicatorView *activityIndicatorView;
}

@end

@implementation ParticipantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    self.navigationItem.title=@"Participants";
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    [activityIndicatorView setCenter:self.view.center];
    [self.view addSubview:activityIndicatorView];
    NSLog(@"%@",_groupid);
    tablealert=0;
    
    [self getparticipants:_groupid];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief get participant list based on groupid. get participant list in dictResponse1 and load the data in table to display.
 *@param value is groupid
 */

-(void) getparticipants:(NSString *)value
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetParticipantList/%@",ipaddress,value];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               tablealert=0;
                                                                               _parttable.hidden=NO;
                                                                               [activityIndicatorView stopAnimating];
                                                                               [_parttable reloadData];
                                                                               
                                                                           });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
/*!
 *@brief get pending participant list based on groupid. get pending participant list in dictResponse1 and load the data in table to display.
 *@param value is groupid
 */
-(void) getpendinglist:(NSString *)value
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetEventPendingList/%@",ipaddress,value];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               if([dictResponse1 isKindOfClass:[NSString class]])
                                                                               {
                                                                                   _parttable.hidden=YES;SCLAlertView *alert = [[SCLAlertView alloc] init];
                                                                                   
                                                                                   [alert showError:self title:@"Sorry"
                                                                                           subTitle:@"User Role is not modurator"
                                                                                   closeButtonTitle:@"OK" duration:0.0f];
                                                                               }
                                                                               else
                                                                               {
                                                                                   tablealert=1;
                                                                                   _parttable.hidden=NO;
                                                                                   [activityIndicatorView stopAnimating];
                                                                                   [_parttable reloadData];
                                                                               }
                                                                           });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    return [dictResponse1 count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    UILabel *name=(UILabel *)[cell viewWithTag:1];
    UILabel *company=(UILabel *)[cell viewWithTag:2];
    
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    
    search = [dictResponse1 objectAtIndex:indexPath.row];
    
    name.text=[search valueForKey:@"UserName"];
    company.text=[search valueForKey:@"CompanyName"];
    return cell;
}
/*!
 *@brief For pending list modurator can accept or reject the user. When select user alert will display. It will so Accept/Reject/Cancel based on modurator selection accept/reject metheod will be call.
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tablealert==1)
    {
        SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
        [alert setHorizontalButtons:YES];
        
        [alert addButton:@"Accept" actionBlock:^(void) {
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
            NSString *urlstr=[NSString stringWithFormat:@"%@/api/event/accept?participantId=%@&eventId=%@&eventType=0",ipaddress,[[dictResponse1 valueForKey:@"UserId"]objectAtIndex:indexPath.row],[[dictResponse1 valueForKey:@"EventId"]objectAtIndex:indexPath.row]];
            
            NSURL * url = [NSURL URLWithString:urlstr];
            
            NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                if(error == nil)
                                                                {
                                                                    
                                                                    dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(),
                                                                                   ^{
                                                                                       [activityIndicatorView startAnimating];
                                                                                       [self getpendinglist:_groupid];
                                                                                       _parttable.hidden=YES;
                                                                                   });
                                                                }
                                                                
                                                            }];
            
            [dataTask resume];
            
        }];
        [alert addButton:@"Reject" actionBlock:^(void) {
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
            NSString *urlstr=[NSString stringWithFormat:@"%@/api/event/reject?participantId=%@&eventId=%@&eventType=0",ipaddress,[[dictResponse1 valueForKey:@"UserId"]objectAtIndex:indexPath.row],[[dictResponse1 valueForKey:@"EventId"]objectAtIndex:indexPath.row]];
            
            NSURL * url = [NSURL URLWithString:urlstr];
            
            NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                if(error == nil)
                                                                {
                                                                    
                                                                    dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                                    NSLog(@"%@",dictresponse3);
                                                                    dispatch_async(dispatch_get_main_queue(),
                                                                                   ^{
                                                                                       [activityIndicatorView startAnimating];
                                                                                       [self getpendinglist:_groupid];
                                                                                       _parttable.hidden=YES;
                                                                                   });
                                                                }
                                                                
                                                            }];
            
            [dataTask resume];
            
        }];
        
        alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/right_answer.mp3", [NSBundle mainBundle].resourcePath]];
        
        [alert showSuccess:@"Action" subTitle:@"Accect or Reject" closeButtonTitle:@"cancel" duration:0.0f];
    }
    
}
/*!
 *@brief Handle All participants amd pending participants by using UISegmentControl
 *@param sender using sender value call the method getparticipants and getpendinglist
 */
- (IBAction)participant_type:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if (selectedSegment == 0) {
        [activityIndicatorView startAnimating];
        [self getparticipants:_groupid];
        _parttable.hidden=YES;
    }
    else{
        [activityIndicatorView startAnimating];
        [self getpendinglist:_groupid];
        _parttable.hidden=YES;
    }
}
@end

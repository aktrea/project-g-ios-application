//
//  GetdataApi.m
//  Project-G
//
//  Created by Sabari on 3/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

#import "GetdataApi.h"

@implementation GetdataApi

+ (NSURLSession *)dataSession {
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    });
    return session;
}

+ (void)fetchContentsOfURL:(NSURL *)url
                completion:(void (^)(NSData *data, NSError *error)) completionHandler {
    
    NSURLSessionDataTask *dataTask =
    [[self dataSession] dataTaskWithURL:url
                      completionHandler:
     
     ^(NSData *data, NSURLResponse *response, NSError *error) {
         
         if (completionHandler == nil) return;
         
         if (error) {
             completionHandler(nil, error);
             return;
         }
         completionHandler(data, nil);
     }];
    
    [dataTask resume];
}
+ (void)postContentsOfURL:(NSURL *)url :(NSDictionary *)dict
               completion:(void (^)(NSData *data, NSError *error)) completionHandler {
    NSError *ss;
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&ss];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [urlRequest setHTTPBody:jsonData];
    NSURLSessionDataTask *dataTask =
    [[self dataSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (completionHandler == nil) return;
        
        if (error) {
            completionHandler(nil, error);
            return;
        }
        completionHandler(data, nil);
    }];
    
    [dataTask resume];
}
@end

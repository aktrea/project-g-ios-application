//
//  ProblemViewController.h
//  Project-G
//
//  Created by Sabari on 16/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProblemViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *problemtable;
@property (strong, nonatomic) IBOutlet UITextField *event_textfield;
@property (strong, nonatomic) IBOutlet UITextField *country_textfield;
@property (strong, nonatomic) IBOutlet UITextView *postcontent_text;
@property (strong, nonatomic) IBOutlet UIButton *solutionpublish_but;
- (IBAction)solutionpublish_but:(id)sender;

@end

//
//  ActivityTableViewCell.h
//  Project-G
//
//  Created by Sabari on 21/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *postedbyname;
@property (strong, nonatomic) IBOutlet UILabel *postedtime;
@property (strong, nonatomic) IBOutlet UIImageView *postedimage;
@property (strong, nonatomic) IBOutlet UILabel *posttext;
@property (strong, nonatomic) IBOutlet UIImageView *postimage;
@property (strong, nonatomic) IBOutlet UIButton *likebut;
@property (strong, nonatomic) IBOutlet UILabel *likelabel;
@property (strong, nonatomic) IBOutlet UILabel *commentlabel;
@property (strong, nonatomic) IBOutlet UIButton *commentbut;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *imgindicator;
@property (strong, nonatomic) IBOutlet UILabel *eventlabel;
@property (strong, nonatomic) IBOutlet UIImageView *starimage;
@end

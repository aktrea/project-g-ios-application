//
//  SolutionViewController.h
//  Project-G
//
//  Created by Sabari on 18/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SolutionViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *solutiontable;

@property id value;
@property NSMutableDictionary *Solutiondata;
@end

//
//  GroupdiscussionViewController.m
//  Project-G
//
//  Created by Sabari on 7/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "GroupdiscussionViewController.h"
#import "HMSegmentedControl.h"


@interface GroupdiscussionViewController ()
@end

@implementation GroupdiscussionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.groupview.hidden = NO;
    self.post.hidden = YES;
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    
    HMSegmentedControl *segmentedControl3 = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Table", @"Post"]];
    [segmentedControl3 setFrame:CGRectMake(0,0,viewWidth,43)];
    [segmentedControl3 setIndexChangeBlock:^(NSInteger index) {
        NSLog(@"Selected index %ld (via block)", (long)index);
        switch (index) {
            case 0:
                self.groupview.hidden = NO;
                self.post.hidden = YES;
                break;
            case 1:
                self.groupview.hidden = YES;
                self.post.hidden = NO;
                break;
                
            default:
                break;
        }
        
    }];
    segmentedControl3.selectionIndicatorHeight = 4.0f;
    segmentedControl3.backgroundColor =[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:0.5];
    segmentedControl3.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
    segmentedControl3.selectionIndicatorColor = [UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1];
    segmentedControl3.selectionIndicatorBoxColor = [UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1];
    segmentedControl3.selectionIndicatorBoxOpacity = 1.0;
    segmentedControl3.selectedTitleTextAttributes=@{NSForegroundColorAttributeName : [UIColor whiteColor]};
    segmentedControl3.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl3.selectedSegmentIndex = 0;
    segmentedControl3.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl3.shouldAnimateUserSelection = NO;
    segmentedControl3.tag = 2;
    [self.seg addSubview:segmentedControl3];
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief display group view and group post view using UISegmentControl(UIContainer).
 */
- (IBAction)segmentValueChanged:(id)sender {
    UISegmentedControl *segment = sender;
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            self.groupview.hidden = NO;
            self.post.hidden = YES;
            break;
        case 2:
            self.groupview.hidden = YES;
            self.post.hidden = NO;
            break;
            
        default:
            break;
    }
    
}
/*!
 *@brief redirection to groupparticipant viewcontroller base on identifier when user select the list button.
 */
- (IBAction)participantlist:(id)sender {
    //grouppartcipant
    [self performSegueWithIdentifier:@"grouppartcipant" sender:self];
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"grouppartcipant"]) {
    }
}
@end

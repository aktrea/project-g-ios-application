//
//  AgendaViewController.m
//  Project-G
//
//  Created by Sabari on 25/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "AgendaViewController.h"
#import "AgendaTableViewCell.h"
#import "EventViewController.h"
#import "DGActivityIndicatorView.h"
#import "Urlclass.h"
#import "GetdataApi.h"
@interface AgendaViewController ()
{
    NSString *ipaddress;
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    NSString *starttime,*endtime;
    DGActivityIndicatorView *activityIndicatorView;
}

@end

@implementation AgendaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    [activityIndicatorView setCenter:self.view.center];
    [self.view addSubview:activityIndicatorView];
    _agendatable.hidden=YES;
    [activityIndicatorView startAnimating];
    
    [self getagendadata];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief get adgenda data from server using NSURLSession and upadte in table.
 */
-(void) getagendadata
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/agendaevent/getagendaevents?id=3",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               [_agendatable reloadData];
                                                                               _agendatable.hidden=NO;      [activityIndicatorView stopAnimating];
                                                                           });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dictResponse count];
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier1 = @"section";
    AgendaTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    UILabel *day=(UILabel *)[headerView viewWithTag:2];
    UILabel *date=(UILabel *)[headerView viewWithTag:1];
    day.text=[NSString stringWithFormat:@"Day %@",[[dictResponse valueForKey:@"Day"]objectAtIndex:section]];
    date.text=[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"DateTime"]objectAtIndex:section]];
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}
/*!
 *@brief display agenda details in table
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell ==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    UILabel *eventname=(UILabel *)[cell viewWithTag:1];
    UILabel *startlabel=(UILabel *)[cell viewWithTag:2];
    UILabel *endlabel=(UILabel *)[cell viewWithTag:3];
    eventname.text=[[dictResponse valueForKey:@"Topic"]objectAtIndex:indexPath.row];
    NSArray *startvalue=[[[dictResponse valueForKey:@"StartTime"]objectAtIndex:indexPath.row] componentsSeparatedByString:@"T"];
    NSArray *endvalue=[[[dictResponse valueForKey:@"EndTime"]objectAtIndex:indexPath.row] componentsSeparatedByString:@"T"];
    starttime=[startvalue objectAtIndex:1];
    endtime=[endvalue objectAtIndex:1];
    startlabel.text=[starttime substringToIndex:[starttime length]-3];
    endlabel.text=[endtime substringToIndex:[endtime length]-3];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
/*!
 *@brief redirection to event page base on groupid
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"eventagenda" sender:[[dictResponse valueForKey:@"GroupId"]objectAtIndex:indexPath.row]];
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //eventagenda
    if([segue.identifier isEqualToString: @"eventagenda"])
    {
        EventViewController *evt=[segue destinationViewController];
        evt.groupid=[sender copy];
    }
}
@end

//
//  SettingsViewController.h
//  Project-G
//
//  Created by Sabari on 9/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end

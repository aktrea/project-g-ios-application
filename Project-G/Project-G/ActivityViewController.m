//
//  ActivityViewController.m
//  Project-G
//
//  Created by Sabari on 16/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ActivityViewController.h"
#import "ActivityTableViewCell.h"
#import "ActivityplainTableViewCell.h"
#import "CommentTableViewCell.h"
#import "CommentViewController.h"
#import "DGActivityIndicatorView.h"
#import "Urlclass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ViewImageVC.h"
//#import "GetdataApi.h"
@interface ActivityViewController ()
{
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    NSString *ipaddress;
    ActivityTableViewCell *cell;
    ActivityplainTableViewCell *cell1;
    UIRefreshControl *refreshControl;
    DGActivityIndicatorView *activityIndicatorView;
    bool likestate;
    int scrollvalue;
    
}
@end

@implementation ActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    scrollvalue=10;
    self.navigationItem.title=@"Activity Feed";
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    [activityIndicatorView setCenter:self.view.center];
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activityfeed:)
                                                 name:@"activityfeed"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activityfeed:)
                                                 name:@"NotificationForLike"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activityfeed:)
                                                 name:@"publicComment"
                                               object:nil];
    
    likestate=false;
    _statustext.delegate=self;
    refreshControl = [[UIRefreshControl alloc]init];
    [self.activitytable addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [self getactivitypost:0 :scrollvalue];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getactivitypost:0 :scrollvalue];
}
/*!
 *@brief when other user post,like,comment this method invoke by NSNotification
 */
- (void)activityfeed:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"activityfeed"]||[[notif name] isEqualToString:@"NotificationForLike"]||[[notif name] isEqualToString:@"publicComment"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getactivitypost:0 :scrollvalue];
        });
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView

{
    
    return YES;
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"status"]) {
        [segue destinationViewController];
    }
    else if ([[segue identifier] isEqualToString:@"comment"])
    {
        CommentViewController *commentsegue= [segue destinationViewController];
        commentsegue.activitydata=[sender copy];
        
    }
    else if ([[segue identifier] isEqualToString:@"imageview"])
    {
        ViewImageVC *img=[segue destinationViewController];
        img.imagedata=[sender copy];
        
    }
    
    
}
/*!
 *@brief getactivitypost by pass intial and end value. dictresponce will get data and assign to tableview to display post.
 */
-(void) getactivitypost:(int)intial :(int)end
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetPosts/%d/%d",ipaddress,intial,end];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            likestate=false;
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               [_activitytable reloadData];
                                                                               [activityIndicatorView stopAnimating];
                                                                           });
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return [dictResponse count];
    }
    else
    {
        return 0;
    }
}
/*!
 *@brief cell is for normal post, cell1 is form image post. Based on document list normal post and image post will be placed.
 */

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"activitycell";
    static NSString *cellidentifier1=@"activitycellplain";
    cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    cell1=[tableView dequeueReusableCellWithIdentifier:cellidentifier1];
    if(cell==nil)
    {
        cell=[[ActivityTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    if(cell1==nil)
    {
        cell1=[[ActivityplainTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier1];
    }
    NSArray *rr;
    if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row] != [NSNull null])
    {
        rr=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row];
    }
    if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row] == [NSNull null]|| rr.count==0 )
    {
        cell1.postbyname.text=[NSString stringWithFormat:@"%@ Posted a Status",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"]objectAtIndex:indexPath.row]];
        if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row]==[NSNull null])
        {
            cell1.eventlabel.text=@"";
        }
        else
        {
            cell1.eventlabel.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row];
        }
        NSString *str = [NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]objectAtIndex:indexPath.row]];
        int moduratorvalue=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"IsModerator"]objectAtIndex:indexPath.row]intValue];
        if(moduratorvalue==1)
        {
            cell1.starimage.hidden=NO;
        }
        else
        {
            cell1.starimage.hidden=YES;
        }
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        
        cell1.postedtime.text = [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        cell1.postcontent.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"]objectAtIndex:indexPath.row];
        cell1.likelabel.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:indexPath.row]];
        NSArray *commentcount=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"]objectAtIndex:indexPath.row];
        cell1.commentlabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]];
        cell1.commentbut.tag=indexPath.row;
        [cell1.commentbut addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        cell1.likebut.tag=indexPath.row;
        [cell1.likebut addTarget:self action:@selector(like: ) forControlEvents:UIControlEventTouchUpInside];
        NSString *like=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]objectAtIndex:indexPath.row]];
        if([like isEqualToString:@"1"])
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell1.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        }
        else
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell1.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            
        }
        
        return cell1;
    }
    else
    {
        NSArray *document=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row];
        NSString *doc=[[document valueForKey:@"documentlink"]objectAtIndex:0];
        NSString *url_Img1 = [NSString stringWithFormat:@"%@%@",ipaddress,doc];
        NSString *url_Img2 = [[document valueForKey:@"documentname"]objectAtIndex:0];
        
        NSString *url_Img_FULL = [url_Img1 stringByAppendingPathComponent:url_Img2];
        cell.imgindicator.hidden=YES;
        cell.postimage.tag = indexPath.row;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(imageTap:)];
        [cell.postimage addGestureRecognizer:tap];
        
        //Enable the image to be clicked
        cell.postimage.userInteractionEnabled = YES;
        
        [cell.postimage sd_setImageWithURL:[NSURL URLWithString:url_Img_FULL]
                          placeholderImage:[UIImage imageNamed:@"Ok Filled-100-3.png"]
                                   options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
        cell.postimage.clipsToBounds = YES;
        
        cell.postedbyname.text=[NSString stringWithFormat:@"%@ Posted a Status",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"]objectAtIndex:indexPath.row]];
        if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row]==[NSNull null])
        {
            cell.eventlabel.text=@"";
        }
        else
        {
            cell.eventlabel.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row];
        }
        int moduratorvalue=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"IsModerator"]objectAtIndex:indexPath.row]intValue];
        if(moduratorvalue==1)
        {
            cell.starimage.hidden=NO;
        }
        else
        {
            cell.starimage.hidden=YES;
        }
        
        NSString *str = [NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]objectAtIndex:indexPath.row]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        cell.postedtime.text = [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        cell.posttext.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"]objectAtIndex:indexPath.row];
        cell.likebut.tag=indexPath.row;
        [cell.likebut addTarget:self action:@selector(like: ) forControlEvents:UIControlEventTouchUpInside];
        NSString *like=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]objectAtIndex:indexPath.row]];
        if([like isEqualToString:@"1"])
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        }
        else
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            
        }
        cell.commentbut.tag=indexPath.row;
        [cell.commentbut addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        cell.likelabel.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:indexPath.row]];
        NSArray *commentcount=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"]objectAtIndex:indexPath.row];
        cell.commentlabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]];
        
        
        return cell;
    }
}
/*!
 *@brief imageTap execute based on UITapGestureRecognizer
 */

- (void)imageTap:(UITapGestureRecognizer *)recognizer
{
    NSArray *document=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:recognizer.view.tag];
    NSString *doc=[[document valueForKey:@"documentlink"]objectAtIndex:0];
    NSString *url_Img1 = [NSString stringWithFormat:@"%@%@",ipaddress,doc];
    NSString *url_Img2 = [[document valueForKey:@"documentname"]objectAtIndex:0];
    
    NSString *url_Img_FULL = [url_Img1 stringByAppendingPathComponent:url_Img2];
    
    [self performSegueWithIdentifier:@"imageview" sender:url_Img_FULL];
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}
/*!
 *@brief handle height for image post and normal post.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row] == [NSNull null] )
    {
        return 175 ;
    }
    else
    {
        return 369;
    }
}
/*!
 *@brief comment will execute when user select comment. Get data based on selection and redirect to comment page
 */
-(void) comment:(id)sender
{
    NSArray *getdataarray=(NSArray *)dictResponse;
    NSDictionary *getactivity=[getdataarray objectAtIndex:[sender tag]];
    [self performSegueWithIdentifier:@"comment" sender:getactivity];
}

/*!
 *@brief user select status column it will redirect to status page.
 */
- (IBAction)navigatebut:(id)sender {
    [self performSegueWithIdentifier:@"status" sender:self];
}
- (void)refreshTable {
    //TODO: refresh your data
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [refreshControl endRefreshing];
                       [self.activitytable reloadData];
                   });
}
/*!
 *@brief stringForTimeIntervalSinceCreated will convert timestamp valu to time like(1 day ago,2 minitutes ago).
 */
- (NSString *)stringForTimeIntervalSinceCreated:(NSDate *)dateTime serverTime:(NSDate *)serverDateTime{
    NSInteger MinInterval;
    NSInteger HourInterval;
    NSInteger DayInterval;
    NSInteger DayModules;
    
    NSInteger interval = labs((NSInteger)[dateTime timeIntervalSinceDate:serverDateTime]);
    if(interval >= 86400)
    {
        DayInterval  = interval/86400;
        DayModules = interval%86400;
        if(DayModules!=0)
        {
            if(DayModules>=3600){
                //HourInterval=DayModules/3600;
                return [NSString stringWithFormat:@"%li days", (long)DayInterval];
            }
            else {
                if(DayModules>=60){
                    //MinInterval=DayModules/60;
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
                else {
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
            }
        }
        else
        {
            return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
        }
        
    }
    
    else{
        
        if(interval>=3600)
        {
            
            HourInterval= interval/3600;
            return [NSString stringWithFormat:@"%li hours ago", (long)HourInterval];
            
        }
        
        else if(interval>=60){
            
            MinInterval = interval/60;
            
            return [NSString stringWithFormat:@"%li minutes ago", (long)MinInterval];
        }
        else{
            return [NSString stringWithFormat:@"%li Sec ago", (long)interval];
        }
        
    }
    
}
/*!
 *@brief handle like and unlike. like and unlike change based on image and send postid to server for like and unlike. like will update based on server response.
 */
-(IBAction)like:(UIButton *)sender
{
    int likecount = 0;
    NSInteger row=[sender tag];
    likecount=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:sender.tag] intValue];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    cell =[_activitytable cellForRowAtIndexPath:indexPath];
    if ([[cell.likebut backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"Like1.png"]])
    {
        if(likestate==false)
        {
            likestate=true;
            likecount--;
            cell.likelabel.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/unlike?postId=%@",ipaddress,[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]objectAtIndex:sender.tag]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       [self getactivitypost:0 :scrollvalue];
                                                                       // [activityIndicatorView startAnimating];
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
        }            NSLog(@"Same");
    }
    else
    {
        if(likestate==false)
        {
            likestate=true;
            cell.likelabel.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/savelike?postId=%@",ipaddress,[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]objectAtIndex:sender.tag]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       [self getactivitypost:0 :scrollvalue];
                                                                       //  [activityIndicatorView startAnimating];
                                                                       
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
        }
        NSLog(@"Not Same");
    }
    
}
/*!
 *@brief get last index from table and call getpost method to get the activity feed increment by 10
 */

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [dictResponse count]-1 ) {
        scrollvalue=scrollvalue+10;
        [self getactivitypost:0 :scrollvalue];
    }
}
/*!
 *@brief add activity indicator at the bottom of the cell when add new activity feed
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    NSArray *visibleRows = [_activitytable visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [_activitytable indexPathForCell:lastVisibleCell];
    if(path.row == [dictResponse count]-1)
    {
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        [spinner startAnimating];
        spinner.frame = CGRectMake(self.view.frame.size.width/2, 0, 0, 44);
        [headerView addSubview:spinner];
    }
    return headerView;
    
}
@end

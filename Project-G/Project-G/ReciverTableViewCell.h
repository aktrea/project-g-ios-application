//
//  ReciverTableViewCell.h
//  Enthiran
//
//  Created by Sabari on 21/9/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReciverTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *recivermessage;
@property (weak, nonatomic) IBOutlet UIImageView *reciverimg;
@property (strong, nonatomic) IBOutlet UILabel *recivertime;
@property (strong, nonatomic) IBOutlet UILabel *receiverpostedby;

@end

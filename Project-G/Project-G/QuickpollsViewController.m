//
//  QuickpollsViewController.m
//  Project-G
//
//  Created by Sabari on 25/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "QuickpollsViewController.h"
#import "QuickpollquestionViewController.h"
#import "DGActivityIndicatorView.h"
#import "CZPicker.h"
#import "CreatepollViewController.h"
#import "Urlclass.h"
@interface QuickpollsViewController ()<CZPickerViewDataSource, CZPickerViewDelegate>
{
    NSString *ipaddress,*intialid;
    NSDictionary *dictResponse, *dictResponse1,*dictresponse2,*dictresponse3;
    DGActivityIndicatorView *activityIndicatorView;
    CZPickerView *eventpick;
    NSInteger rows1,selectrow;
    NSMutableArray *challengeid,*groupid;
}

@end

@implementation QuickpollsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    self.navigationItem.title=@"Polls";
    challengeid=[[NSMutableArray alloc]init];
    groupid=[[NSMutableArray alloc]init];
    intialid=@"0";
    _createpollview.hidden=YES;
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    _grouptextfield.delegate=self;
    eventpick = [[CZPickerView alloc] initWithHeaderTitle:@"Select Group" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Select"];
    [activityIndicatorView setCenter:self.view.center];
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    [self getgroups];
    [self getpolls];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getpolls];
    [self getgroups];
    _polltable.hidden=YES;
    [activityIndicatorView startAnimating];
}
/*!
 *@brief get groups from server using NSURLSession
 */
-(void)getgroups
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/user/groups",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}
/*!
 *@brief get poll from server using NSURLSession. intial 0 means it will get all polls. If we pass groupid it will filter based on group.
 */
-(void)getpolls
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/quickpoll/getpollquestions?id=%@",ipaddress,intialid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               _polltable.hidden=NO;
                                                                               [_polltable reloadData];
                                                                               [activityIndicatorView stopAnimating];
                                                                               for(int i=0;i<dictResponse.count;i++)
                                                                               {
                                                                                   [challengeid addObject:[[dictResponse valueForKey:@"Challengeid"]objectAtIndex:i]];
                                                                                   [groupid addObject:[[dictResponse valueForKey:@"GroupId"]objectAtIndex:i]];
                                                                                   
                                                                               }
                                                                           });
                                                            
                                                        }
                                                        
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dictResponse count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    UILabel *question=(UILabel *)[cell viewWithTag:1];
    UILabel *name=(UILabel *)[cell viewWithTag:2];
    question.text=[[dictResponse valueForKey:@"QuestionText"]objectAtIndex:indexPath.row];
    name.text=[[dictResponse valueForKey:@"CreatedBy"]objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
/*!
 *@brief adjust quick poll cell based on poll question content.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [[dictResponse valueForKey:@"QuestionText"]objectAtIndex:indexPath.row];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGRect textRect = [str boundingRectWithSize:CGSizeMake(280,999)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSParagraphStyleAttributeName: paragraphStyle.copy}
                                        context:nil];
    CGSize size= textRect.size;
    return size.height +80;
}
/*!
  *@brief send poll data based on user selection
  */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *polldata=(NSArray *)dictResponse;
    [self performSegueWithIdentifier:@"questionpoll" sender:[polldata objectAtIndex:indexPath.row]];
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString: @"questionpoll"])
    {
        QuickpollquestionViewController *quepoll= [segue destinationViewController];
        quepoll.getpoll=[sender copy];
    }
    else if([segue.identifier isEqualToString: @"createpoll"])
    {
        [segue destinationViewController];
    }
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    eventpick.delegate = self;
    eventpick.dataSource = self;
    eventpick.needFooterView = YES;
    [self getgroups];
    [eventpick show];
    return NO;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    eventpick.delegate = self;
    eventpick.dataSource = self;
    eventpick.needFooterView = YES;
    [self getgroups];
    [eventpick show];
    return YES;
    
}
/*!
 *@brief show group name in czpickerview
 */

- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:[dictresponse3 valueForKey:@"GroupName"][row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    
    return [dictresponse3 valueForKey:@"GroupName"] [row];
}
- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    
    return [[dictresponse3 valueForKey:@"GroupName"] count];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row {
    
    _grouptextfield.text=[dictresponse3 valueForKey:@"GroupName"] [row];
    intialid=[dictresponse3 valueForKey:@"GroupId"] [row];
    [self getpolls];
    _polltable.hidden=YES;
    [activityIndicatorView startAnimating];
    
    
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows {
    for (NSNumber *n in rows)
    {
        rows1 = [n integerValue];
    }
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    
    NSLog(@"^Canceled.");
}

- (IBAction)createpoll:(id)sender {
    [self performSegueWithIdentifier:@"createpoll" sender:self];
}


@end

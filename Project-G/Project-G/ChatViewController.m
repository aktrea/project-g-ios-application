//
//  ChatViewController.m
//  Enthiran
//
//  Created by Sabari on 21/9/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ChatViewController.h"
#import "SenderTableViewCell.h"
#import "ReciverTableViewCell.h"
#import "DGActivityIndicatorView.h"

@interface ChatViewController ()
{
    NSMutableArray *dummychat;
    CGSize size;
    NSIndexPath *selectedindexpath;
    UIImageOrientation scrollOrientation;
    CGPoint lastPos;
    NSString *ipaddress;
    int userid;
    DGActivityIndicatorView *activityIndicatorView;
    // DesisionViewController *dec;
    NSDictionary *dictResponse,*dictResponse1;
    NSMutableArray *chatdata,*groupchatdata;
    
    
    
}
@property (weak, nonatomic) IBOutlet UITextField *chatTxt;

@end

@implementation ChatViewController
@synthesize val,val1,val2,touserid,touseridfrommsg,chatmessage;

- (void)viewDidLoad {
    [super viewDidLoad];
    if(_frmvalue.length==0)
    {
        self.navigationItem.title =_groupname;
    }else
    {
        self.navigationItem.title =_frmvalue;
    }
    _chatTxt.textColor=[UIColor blackColor];
    [[NSUserDefaults standardUserDefaults] setValue:touserid forKey:@"toid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recivemessage:)
                                                 name:@"recivemessage"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivegroupmessage:)
                                                 name:@"receivegroupmessage"
                                               object:nil];
    
    
#pragma mark - activity indicator to display when loading
    val=[[NSMutableArray alloc]init];
    val1=[[NSMutableArray alloc]init];
    val2=[[NSMutableArray alloc]init];
    _groupchatmessage=[[NSMutableArray alloc]init];
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    
    [activityIndicatorView setCenter:self.view.center];
    
    [self.view addSubview:activityIndicatorView];
    
    [activityIndicatorView startAnimating];
    
    _chatTxt.delegate=self;
    _chattable.backgroundColor=[UIColor clearColor];
    //dec=[[DesisionViewController alloc]init];
    [val addObject:[NSString stringWithFormat:@"%@",_uservalue]];
    [val1 addObject:[NSNumber numberWithInt:userid]];
    
    NSString *userid1=[[NSUserDefaults standardUserDefaults]stringForKey:@"userid"];
    userid=[userid1 intValue];
    //ipaddress=@"192.168.3.127/akton";
    ipaddress=@"aktrea.com/fintech";
    //ipaddress=@"aktrea.com/Enthiran";
    //    if(_getstatus==true)
    //    {
    [self getdata];
    //    }
    //    else
    //    {
    [self getgroupmessage];
    //    }
    
}
/*!
 *@brief get individual chat based on from userid and touserid
 */
-(void)getdata
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/chat/getChat/%d/%@/20",ipaddress,userid,touserid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               
                                                                               chatdata=(NSMutableArray *)dictResponse;
                                                                               chatmessage=[chatdata valueForKey:@"message"];
                                                                               [_chattable reloadData];
                                                                               [self performSelector:@selector(goToBottom) withObject:nil afterDelay:0.3];
                                                                               [activityIndicatorView stopAnimating];
                                                                           });
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
/*!
 *@brief get group message based on groupid
 */
-(void)getgroupmessage
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/chat/getGroup/%@",ipaddress,_groupid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            groupchatdata=(NSMutableArray *)dictResponse1;
                                                            [_chattable reloadData];
                                                            [self performSelector:@selector(goToBottom) withObject:nil afterDelay:0.2];
                                                            [activityIndicatorView stopAnimating];
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}

- (void)sendnotification:(id)connectionid User1:(NSString *)username1  User2:(NSString *)username2 Message:(NSString *)msg DateHelper:(NSString *)date FrmName:(NSString *)frm_user fromid:(id)frm_id
{
    //    NSLog(@"%@",connectionid);
    //        NSLog(@"%@",username1);
    //            NSLog(@"%@",msg);
    //            NSLog(@"%@",username2);
    //    NSLog(@"%@",date);
    //    NSLog(@"%@",frm_user);
    //    NSLog(@"%@",frm_id);
    
}
/*!
 *@brief get private message from signalr NSNotifiaction
 */
- (void)sendPrivateMessage:(id)connectionid fromUsername:(NSString *)fromusername  Message:(NSString *)message Tousername:(NSString *)tousername DateHelper:(NSString *)date FrmName:(NSString *)frm_user Toid:(id)to_id
{
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",to_id] forKey:@"toid1"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSArray *keys = [NSArray arrayWithObjects:@"Name",@"PostedAt",@"frm",@"message",@"to", nil];
    NSArray *objects = [NSArray arrayWithObjects:frm_user,date,fromusername,message,tousername, nil];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [[NSUserDefaults standardUserDefaults] setObject:dictionary forKey:@"sendprivatemessage"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"recivemessage" object:self];
    
    
}
/*!
 *@brief get group message from signalr NSNotifiaction
 */
-(void)GroupMessage:(NSString *)uname Message:(NSString *)groupMessage groupid:(NSString*)Groupid Gname:(NSString *)gname DateHelper:(NSString *)date
{
    NSArray *keys = [NSArray arrayWithObjects:@"ID",@"Message",@"PostedAt",@"PostedBy", nil];
    NSArray *objects = [NSArray arrayWithObjects:Groupid,groupMessage,date,uname, nil];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [[NSUserDefaults standardUserDefaults] setObject:dictionary forKey:@"GroupMessage"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"receivegroupmessage" object:self];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //    [TSMessage setDefaultViewController:self];
    //    [TSMessage setDelegate:self];
    
    
    [self getdata];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marks -Hide Status bar


-(BOOL)prefersStatusBarHidden{
    return YES;
}



#pragma marks -View adjust based keyboard appears

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self sendmsg];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}
/*!
 *@brief when keyboard appear reduce the view y value to appears in top
 */
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-200,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}
/*!
 *@brief when keyboard disappear  view wiil be normal
 */
-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}



#pragma marks -Tableview Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_getstatus==true)
    {
        return chatdata.count;
    }
    else
    {
        return groupchatdata.count;
    }
}
/*!
 *@brief update cell size based on message content
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str;
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    if(_getstatus==true)
    {
        str = [[chatdata objectAtIndex:indexPath.row]valueForKey:@"message"];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        CGRect textRect = [str boundingRectWithSize:CGSizeMake(280,999)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSParagraphStyleAttributeName: paragraphStyle.copy}
                                            context:nil];
        size=textRect.size;
    }else if (_getstatus ==false)
    {
        str = [[groupchatdata objectAtIndex:indexPath.row]valueForKey:@"Message"];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        CGRect textRect = [str boundingRectWithSize:CGSizeMake(280,999)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSParagraphStyleAttributeName: paragraphStyle.copy}
                                            context:nil];
        size=textRect.size;
    }
    
    if(size.height < 24)
    {
        return size.height +35;
    }
    else if(size.height < 40)
    {
        return size.height +65;
    }
    else if(size.height < 60)
    {
        return size.height +85;
    }
    else if(size.height < 70)
    {
        return size.height +100;
    }
    else
    {
        return size.height + 130;
    }
}
/*!
 *@brief when keyboard disappear  view wiil be normal
 */
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (tableView.isDragging) {
        UIView *myView = cell.contentView;
        CALayer *layer = myView.layer;
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        rotationAndPerspectiveTransform.m34 = 1.0 / -1000;
        if (scrollOrientation == UIImageOrientationDown) {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI*0.5, 1.0f, 0.0f, 0.0f);
        } else {
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, -M_PI*0.5, 1.0f, 0.0f, 0.0f);
        }
        layer.transform = rotationAndPerspectiveTransform;
        [UIView animateWithDuration:.8 animations:^{
            layer.transform = CATransform3DIdentity;
        }];
    }
    
}
- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    scrollOrientation = scrollView.contentOffset.y > lastPos.y?UIImageOrientationDown:UIImageOrientationUp;
    lastPos = scrollView.contentOffset;
    [self.view endEditing:YES];
    
}
/*!
 *@brief tableview scroll to up for new message display
 */
-(void)tablescrollpoint
{
    NSInteger lastSectionIndex = MAX(0, [self.chattable numberOfSections] - 1);
    NSInteger lastRowIndex = MAX(0, [self.chattable numberOfRowsInSection:lastSectionIndex] - 1);
    
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex];
    [self.chattable scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    
}
#pragma marks -Tableview Scroll to bottom
-(void)goToBottom
{
    if(_getstatus==true)
    {
        if(chatdata.count>9)
        {
            [self tablescrollpoint];
        }
    }
    else
    {
        if(groupchatdata.count>9)
        {
            [self tablescrollpoint];
        }
        
    }
    
    
}
/*!
 *@brief display message based on from user and to user
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier1 = @"SenderCell";
    static NSString *CellIdentifier2 = @"ReciverCell";
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //if (indexPath.row % 2 == 1)
    // NSLog(@"%@",[[chatdata valueForKey:@"frm"]objectAtIndex:indexPath.row]);
    if(_getstatus==true)
    {
        if([[[chatdata valueForKey:@"frm"]objectAtIndex:indexPath.row] isEqualToString:_frmvalue])
            
        {
            ReciverTableViewCell *cell = (ReciverTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
            cell.recivermessage.text=[NSString stringWithFormat:@"%@",[[chatdata valueForKey:@"message"]objectAtIndex:indexPath.row]];
            cell.recivertime.text=[NSString stringWithFormat:@"%@",[[chatdata valueForKey:@"PostedAt"]objectAtIndex:indexPath.row]];
            cell.receiverpostedby.text=@"";
            // cell.recivermessage.text=[chatmessage objectAtIndex:indexPath.row];
            
            cell.recivermessage.layer.cornerRadius=10;
            cell.recivermessage.layer.masksToBounds=YES;
            cell.reciverimg.layer.cornerRadius=23;
            cell.reciverimg.layer.masksToBounds=YES;
            // Configure cell
            cell.backgroundColor=[UIColor clearColor];
            return cell;
            
            
            
            
        }
        
        else {
            SenderTableViewCell *cell = (SenderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            //        cell.sendermessage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"speech1.png"]];
            cell.sendermessage.layer.cornerRadius=10;
            cell.sendermessage.layer.masksToBounds=YES;
            NSString *msg=[NSString stringWithFormat:@"%@",[[chatdata valueForKey:@"message"]objectAtIndex:indexPath.row]];
            msg=[self htmlEntityDecode:msg];
            cell.sendermessage.text=msg;
            cell.sendertime.text=[NSString stringWithFormat:@"%@",[[chatdata valueForKey:@"PostedAt"]objectAtIndex:indexPath.row]];
            cell.senderpostedby.text=@"";
            
            // cell.sendermessage.text=[chatmessage objectAtIndex:indexPath.row];
            cell.senderimage.layer.cornerRadius=23;
            cell.senderimage.layer.masksToBounds=YES;
            cell.backgroundColor=[UIColor clearColor];
            
            return cell;
        }
    }
    else
    {
        if([[[groupchatdata valueForKey:@"PostedBy"]objectAtIndex:indexPath.row] isEqualToString:[[NSUserDefaults standardUserDefaults]stringForKey:@"Currentuser"]])
            
        {
            SenderTableViewCell *cell = (SenderTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
            //        cell.sendermessage.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"speech1.png"]];
            cell.sendermessage.layer.cornerRadius=10;
            cell.sendermessage.layer.masksToBounds=YES;
            NSString *msg1=[NSString stringWithFormat:@"%@",[[groupchatdata valueForKey:@"Message"]objectAtIndex:indexPath.row]];
            msg1=[self htmlEntityDecode:msg1];
            
            cell.sendermessage.text=msg1;
            cell.sendertime.text=[NSString stringWithFormat:@"%@",[[groupchatdata valueForKey:@"PostedAt"]objectAtIndex:indexPath.row]];
            cell.senderpostedby.text=@"";
            
            // cell.sendermessage.text=[chatmessage objectAtIndex:indexPath.row];
            cell.senderimage.layer.cornerRadius=23;
            cell.senderimage.layer.masksToBounds=YES;
            cell.backgroundColor=[UIColor clearColor];
            
            return cell;
        }
        
        else {
            
            ReciverTableViewCell *cell = (ReciverTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier2];
            cell.recivermessage.text=[NSString stringWithFormat:@"%@",[[groupchatdata valueForKey:@"Message"]objectAtIndex:indexPath.row]];
            // cell.recivermessage.text=[chatmessage objectAtIndex:indexPath.row];
            cell.recivertime.text=[NSString stringWithFormat:@"%@",[[groupchatdata valueForKey:@"PostedAt"]objectAtIndex:indexPath.row]];
            cell.receiverpostedby.text=[NSString stringWithFormat:@"%@",[[groupchatdata valueForKey:@"PostedBy"]objectAtIndex:indexPath.row]];
            cell.recivermessage.layer.cornerRadius=10;
            cell.recivermessage.layer.masksToBounds=YES;
            cell.reciverimg.layer.cornerRadius=23;
            cell.reciverimg.layer.masksToBounds=YES;
            // Configure cell
            cell.backgroundColor=[UIColor clearColor];
            return cell;
            
        }
        
    }
}


- (void)recivemessage:(NSNotification *)notif  {
    
    if ([[notif name] isEqualToString:@"recivemessage"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            
            if([[[NSUserDefaults standardUserDefaults]stringForKey:@"toid"]integerValue] ==[[[NSUserDefaults standardUserDefaults]stringForKey:@"toid1"]integerValue])
            {
                [self.view endEditing:YES];
                [chatdata addObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"sendprivatemessage"]];
                [_chattable reloadData];
                [self performSelector:@selector(goToBottom) withObject:nil afterDelay:0.2];
                
                
            }
            
        });
        
    }
}

- (void)receivegroupmessage:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"receivegroupmessage"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view endEditing:YES];
            [groupchatdata addObject:[[NSUserDefaults standardUserDefaults]valueForKey:@"GroupMessage"]];
            [_chattable reloadData];
            [self performSelector:@selector(goToBottom) withObject:nil afterDelay:0.2];
            
            
        });
        
    }
}


#pragma marks - Send button
- (IBAction)send:(id)sender {
    [self sendmsg];
}
-(void)sendmsg
{
    if(_chatTxt.text.length !=0)
    {
        if(_getstatus ==true)
        {
            [val addObject:_chatTxt.text];
            [[NSUserDefaults standardUserDefaults] setObject:val forKey:@"chatmessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendmessage" object:self];
            [val removeLastObject];
        }
        else
        {
            int useridtogroup=[[[NSUserDefaults standardUserDefaults]stringForKey:@"userid"]intValue];
            int groupidtogroup=[_groupid intValue];
            NSArray *g_messgage=[NSArray arrayWithObjects:[[NSUserDefaults standardUserDefaults]stringForKey:@"Currentuser"],_chatTxt.text,[NSNumber numberWithInt:useridtogroup],[NSNumber numberWithInt:groupidtogroup],_groupname,nil];
            [_groupchatmessage addObjectsFromArray:g_messgage];
            [[NSUserDefaults standardUserDefaults] setObject:_groupchatmessage forKey:@"groupchatmessage"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendgroupmessage" object:self];
            [_groupchatmessage removeAllObjects];
        }
        _chatTxt.text=@"";
        [_chatTxt resignFirstResponder];
        
    }
    
}
-(NSString *)htmlEntityDecode:(NSString *)string
{
    string = [string stringByReplacingOccurrencesOfString:@"&#33;" withString:@"\""];
    string = [string stringByReplacingOccurrencesOfString:@"&#39;" withString:@"'"];
    string = [string stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    string = [string stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    string = [string stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]; // Do this last so that, e.g. @"&amp;lt;" goes to @"&lt;" not @"<"
    
    return string;
}

@end

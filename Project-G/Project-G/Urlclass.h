//
//  Urlclass.h
//  Pods
//
//  Created by Sabari on 3/1/17.
//
//

#import <Foundation/Foundation.h>

@interface Urlclass : NSObject
{
    NSString *urlstring;
}

@property (nonatomic, retain) NSString *urlstring;

+ (id)sharedManager;



@end

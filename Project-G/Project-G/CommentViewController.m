//
//  CommentViewController.m
//  Project-G
//
//  Created by Sabari on 24/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "CommentViewController.h"
#import "CommentTableViewCell.h"
#import "SCLAlertView.h"
#import "Urlclass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ViewImageVC.h"
@interface CommentViewController ()
{
    
    CommentTableViewCell *cell;
    NSMutableDictionary *dictResponse, *dictResponse1,*dictresponse3;
    NSString *ipaddress,*commentvalue,*url_Img_FULL;
    NSArray *getcopy;
    int commentstatus,comtcount;
    bool likestate;
}

@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    self.navigationItem.title=@"Comment";
    _commenttable.hidden=YES;
    _commentview.hidden=YES;
    commentstatus=0;
    likestate=false;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(publicComment:)
                                                 name:@"publicComment"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(publicComment:)
                                                 name:@"NotificationForLike"
                                               object:nil];
    dictResponse=_activitydata;
    [self assign];
    //    [self getactivitypost];
    // Do any additional setup after loading the view.
}
/*!
 *@brief when other user like,comment this method invoke by NSNotification and update comments and like.
 */
- (void)publicComment:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"publicComment"])
    {
        NSDictionary* activitydata = notif.userInfo;
        NSMutableArray *sss=[[dictResponse valueForKey:@"Posts"] valueForKey:@"CommentsList"];
        [sss addObject:activitydata];
        _comment.text=@"";
        commentstatus=0;
        dispatch_async(dispatch_get_main_queue(), ^{
            [_commenttable reloadData];
            ++comtcount;
            _commentlabel.text=[NSString stringWithFormat:@"%d",comtcount];
        });
        
    }
    else if([[notif name] isEqualToString:@"NotificationForLike"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getactivitypost];
        });
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief getactivitypost for update like and unlike */
-(void) getactivitypost
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetPosts/0/20",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            likestate=false;
                                                            dictResponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            getcopy=(NSArray *)dictResponse1;
                                                            dictResponse=[getcopy objectAtIndex:[_value integerValue]];
                                                            
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               [self assign];
                                                                           });
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
/*!
 *@brief get data and seperate normal post and image post based on documents.
 */

-(void)assign
{
    if([[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"] == [NSNull null] )
    {
        _postimage.hidden=YES;
        _postbyname.text=[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"];
        NSString *str = [NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        
        _posttime.text = [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        _postcontent.text=[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"];
        _likelabel.text=[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]];
        NSArray *commentcount=[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"];
        _commentlabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]];
        comtcount=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]].intValue;
        NSString *like=[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]];
        if([like isEqualToString:@"1"])
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [_likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        }
        else
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [_likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            
        }
    }
    else
    {
        NSArray *document=[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"];
        NSString *doc=[[document valueForKey:@"documentlink"]objectAtIndex:0];
        NSString *url_Img1 = [NSString stringWithFormat:@"%@%@",ipaddress,doc];
        NSString *url_Img2 = [[document valueForKey:@"documentname"]objectAtIndex:0];
        url_Img_FULL = [url_Img1 stringByAppendingPathComponent:url_Img2];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(imageTap:)];
        [_postimage addGestureRecognizer:tap];
        
        //Enable the image to be clicked
        _postimage.userInteractionEnabled = YES;
        [_postimage sd_setImageWithURL:[NSURL URLWithString:url_Img_FULL]placeholderImage:[UIImage imageNamed:@"placeholder.png"]options:SDWebImageRefreshCached];
        _postimage.clipsToBounds = YES;
        
        _postbyname.text=[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"];
        NSString *str = [NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        
        _posttime.text = [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        _postcontent.text=[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"];
        _likelabel.text=[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]];
        NSArray *commentcount=[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"];
        _commentlabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]];
        comtcount=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]].intValue;
        NSString *like=[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]];
        if([like isEqualToString:@"1"])
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [_likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        }
        else
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [_likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            
        }
        
    }
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [_commenttable reloadData];
                       
                       _commenttable.hidden=NO;
                       _commentview.hidden=NO;
                   });}
/*!
 *@brief redirect to ViewImageVC when user select the image.
 */

- (void)imageTap:(UITapGestureRecognizer *)recognizer
{
    
    [self performSegueWithIdentifier:@"imageview" sender:url_Img_FULL];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    
    return [[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"] count ];
    
    
}
/*!
 *@brief display and update comments in tableview
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellidentifier2=@"comment3";
    cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier2];
    if(cell==nil)
    {
        cell=[[CommentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier2];
    }
    NSArray *comment=[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"];
    cell.postedbyname.text=[[comment valueForKey:@"CommentedByName"]objectAtIndex:indexPath.row];
    NSString *str = [NSString stringWithFormat:@"%@",[[comment valueForKey:@"CommentedOn"]objectAtIndex:indexPath.row]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
    NSDate *currentDate = [dateFormatter dateFromString:str];
    cell.posttime.text= [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
    cell.commenttext.text=[[comment valueForKey:@"CommentText"]objectAtIndex:indexPath.row];
    
    return cell;
    
}
/*!
 *@brief adjust cell size based on comment contents.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *comment=[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"];
    NSString *str = [[comment valueForKey:@"CommentText"]objectAtIndex:indexPath.row];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGRect textRect = [str boundingRectWithSize:CGSizeMake(280,999)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSParagraphStyleAttributeName: paragraphStyle.copy}
                                        context:nil];
    CGSize size= textRect.size;
    return size.height +85;
}
/*!
 *@brief stringForTimeIntervalSinceCreated will convert timestamp valu to time like(1 day ago,2 minitutes ago).
 */
- (NSString *)stringForTimeIntervalSinceCreated:(NSDate *)dateTime serverTime:(NSDate *)serverDateTime{
    NSInteger MinInterval;
    NSInteger HourInterval;
    NSInteger DayInterval;
    NSInteger DayModules;
    
    NSInteger interval = labs((NSInteger)[dateTime timeIntervalSinceDate:serverDateTime]);
    if(interval >= 86400)
    {
        DayInterval  = interval/86400;
        DayModules = interval%86400;
        if(DayModules!=0)
        {
            if(DayModules>=3600){
                //HourInterval=DayModules/3600;
                return [NSString stringWithFormat:@"%li days", (long)DayInterval];
            }
            else {
                if(DayModules>=60){
                    //MinInterval=DayModules/60;
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
                else {
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
            }
        }
        else
        {
            return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
        }
        
    }
    
    else{
        
        if(interval>=3600)
        {
            
            HourInterval= interval/3600;
            return [NSString stringWithFormat:@"%li hours ago", (long)HourInterval];
            
        }
        
        else if(interval>=60){
            
            MinInterval = interval/60;
            
            return [NSString stringWithFormat:@"%li minutes ago", (long)MinInterval];
        }
        else{
            return [NSString stringWithFormat:@"%li Sec ago", (long)interval];
        }
        
    }
    
}
/*!
 *@brief handle like and unlike. like and unlike change based on image and send postid to server for like and unlike. like will update based on server response.
 */
- (IBAction)likebut:(id)sender {
    int likecount=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"] intValue];
    if ([[_likebut backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"Like1.png"]]){
        if(likestate==false)
        {
            likestate=true;
            likecount--;
            if(likecount<0)
            {
                likecount=0;
            }
            
            likecount--;
            _likelabel.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [_likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/unlike?postId=%@",ipaddress,[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       [self getactivitypost];
                                                                       // [activityIndicatorView startAnimating];
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
            
        }
        
        NSLog(@"Same image");
    }else{
        if(likestate==false)
        {
            likestate=true;        likecount++;
            _likelabel.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [_likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/savelike?postId=%@",ipaddress,[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       [self getactivitypost];
                                                                       //[activityIndicatorView startAnimating];
                                                                       
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
        }
        NSLog(@"not Same image");
    }
    
}

- (IBAction)commentbut:(id)sender {
    commentvalue=_comment.text;
    _comment.text=@"";
    [self postcomment];
}
/*!
 *@brief handle post solution by user. commentstatus==0(avoid duplicate post) will allow the post.If textfielf empty it will show alert.
 */-(void) postcomment
{
    if(commentstatus==0)
    {
        commentstatus=1;
        
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
        [self.view endEditing:YES];
        if(commentvalue.length==0)
        {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            
            SCLTextView *evenField = [alert addTextField:@"Enter your Comment"];
            evenField.keyboardType = UIKeyboardTypeDefault;
            
            
            [alert addButton:@"Post" validationBlock:^BOOL{
                if (evenField.text.length == 0)
                {
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Sorry"
                                                  message:@"Please enter the Solution"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* okbutton = [UIAlertAction
                                               actionWithTitle:@"Ok"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   _comment.text=@"";
                                                   commentstatus=0;
                                                   
                                                   
                                               }];
                    
                    
                    [alert addAction:okbutton];
                    
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    [evenField becomeFirstResponder];
                    commentstatus=0;
                    return NO;
                }
                
                
                
                return YES;
            } actionBlock:^{
                commentvalue=evenField.text;
                [self postcommentvalue];
            }];
            
            [alert showEdit:self title:@"Post Solution" subTitle:@"You can type your solution here" closeButtonTitle:@"Cancel" duration:0];
            
        }
        else{
            //cell.postbut.userInteractionEnabled=NO;
            // [activityIndicatorView startAnimating];
            [self postcommentvalue];
            
        }
    }
}
/*!
 *@brief post comment, solutiondata(NSDictionary) in post method and update comment data based on responce.
 */
-(void)postcommentvalue
{
    NSDictionary *solutiondata = [NSDictionary dictionaryWithObjectsAndKeys:commentvalue,@"CommentText",[[dictResponse valueForKey:@"Posts" ] valueForKey:@"Id"],@"PostId",nil];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSError *ss;
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/Post/InsertComment",ipaddress]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:solutiondata options:NSJSONWritingPrettyPrinted error:&ss];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [urlRequest setHTTPBody:jsonData];
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           if(error == nil)
                                                           {
                                                               
                                                               dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                               dispatch_async(dispatch_get_main_queue(),
                                                                              ^{
                                                                                  NSMutableArray *sss=[[dictResponse valueForKey:@"Posts"] valueForKey:@"CommentsList"];
                                                                                  [sss addObject:dictresponse3];
                                                                                  // [dictResponse setObject:sss forKey:@"CommentsList"];
                                                                                  _comment.text=@"";
                                                                                  commentstatus=0;
                                                                                  [_commenttable reloadData];
                                                                                  ++comtcount;
                                                                                  _commentlabel.text=[NSString stringWithFormat:@"%d",comtcount];
                                                                              });
                                                           }
                                                           else{
                                                               NSLog(@"Error %@",[error localizedDescription]);
                                                           }
                                                           
                                                       }];
    
    [dataTask resume];
    
    
    
    
    
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"imageview"])
    {
        ViewImageVC *img=[segue destinationViewController];
        img.imagedata=[sender copy];
    }
}

@end


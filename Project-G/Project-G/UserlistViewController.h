//
//  UserlistViewController.h
//  Project-G
//
//  Created by Sabari on 9/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserlistViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property (strong, nonatomic) IBOutlet UITableView *userlistTable;
@property(strong,nonatomic)NSMutableArray *val;
@property(retain,nonatomic)NSArray *memberlist;
@property(retain,nonatomic)NSArray*uservalue;
@property(retain,nonatomic)NSArray*touserid;
@property(retain,nonatomic)NSArray *groupname;
@property(retain,nonatomic)NSArray*groupid;
@end

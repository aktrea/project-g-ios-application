//
//  SearchViewController.m
//  Project-G
//
//  Created by Sabari on 17/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "SearchViewController.h"
#import "EventViewController.h"
#import "ProfileViewController.h"
#import "Urlclass.h"

@interface SearchViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    NSString *ipaddress,*searchstring;
    
}

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    _searchbar.delegate=self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief when user enter the user name or event name getsearchlist function execute.
 *searchText is user entered text.
 */
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"%@",searchText);
    if([searchText isEqualToString:@""])
    {
        dictResponse=nil;
        [_searchtable reloadData];
    }
    else
    {
    [self getsearchlist:searchText];
    [_searchtable reloadData];
    }
}

/*!
*@brief get serach data from server and load data in table to display.
*searcstr is user entered text.
*/
-(void) getsearchlist:(NSString *)searchstr
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/SearchUsers?searchTerm=%@",ipaddress,searchstr];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               
                                                                               [_searchtable reloadData];
                                                                           });
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if (dictResponse)
    {
        _searchtable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        _searchtable.backgroundView = nil;
    }
    else
    {
        UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _searchtable.bounds.size.width, _searchtable.bounds.size.height)];
        noDataLabel.text             = @"No data available";
        noDataLabel.textColor        = [UIColor blackColor];
        noDataLabel.textAlignment    = NSTextAlignmentCenter;
        _searchtable.backgroundView = noDataLabel;
        _searchtable.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return numOfSections;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dictResponse count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        
    }
    cell.textLabel.text=[[dictResponse valueForKey:@"UserNameOrEventName"]objectAtIndex:indexPath.row];
    int event=[[[dictResponse valueForKey:@"IsEvent"]objectAtIndex:indexPath.row]intValue];
    int user=[[[dictResponse valueForKey:@"IsUser"]objectAtIndex:indexPath.row]intValue];
    if(event==1)
    {
        cell.detailTextLabel.text=@"Event";
    }
    if(user==1)
    {
        cell.detailTextLabel.text=@"User";
    }
    return cell;
}
/*!
 *@brief When user select user name it will display the user details. If event it will redirect to event page.
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int event=[[[dictResponse valueForKey:@"IsEvent"]objectAtIndex:indexPath.row]intValue];
    int user=[[[dictResponse valueForKey:@"IsUser"]objectAtIndex:indexPath.row]intValue];
    if(event==1)
    {
        [self performSegueWithIdentifier:@"searchevent" sender:[[dictResponse valueForKey:@"Id"] objectAtIndex:indexPath.row]];
    }
    if(user==1)
    {
        [self performSegueWithIdentifier:@"searchuser" sender:[[dictResponse valueForKey:@"Id"] objectAtIndex:indexPath.row]];
    }
    
}
/*!
 *@brief redirection based on selection using identifier
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"searchevent"])
    {
        EventViewController *event=[segue destinationViewController];
        event.groupid=[sender copy];
    }
    else if([segue.identifier isEqualToString:@"searchuser"])
    {
        ProfileViewController *profile=[segue destinationViewController];
        profile.groupid=[sender copy];
    }
}
@end

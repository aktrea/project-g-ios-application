//
//  LoginViewController.h
//  Enthiran
//
//  Created by Sabari on 5/10/16.
//  Copyright © 2016 aktrea. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>
{
    
    NSMutableData *receivedDataLogin;
    
}


@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (nonatomic,retain) NSURLConnection *loginConnection;
@property (nonatomic,retain) NSMutableArray *loginJsonData;
@property (weak, nonatomic) IBOutlet UITextField *usernameText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
- (IBAction)submitButton:(id)sender;



@end

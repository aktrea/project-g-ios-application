//
//  LoginViewController.m
//  Enthiran
//
//  Created by Sabari on 5/10/16.
//  Copyright © 2016 aktrea. All rights reserved.
//


#import "LoginViewController.h"
#import "SCLAlertView.h"
#import "DGActivityIndicatorView.h"
#import "Urlclass.h"



@interface LoginViewController ()
{
    NSString *ipaddress;
}

@end

@implementation LoginViewController
{
    DGActivityIndicatorView *activityIndicatorView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    NSLog(@"%@",ipaddress);
    _usernameText.delegate = self;
    _usernameText.delegate = self;
    [_usernameText setLeftViewMode:UITextFieldViewModeAlways];
    _usernameText.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Circled User Male-25.png"]];
    [_passwordText setLeftViewMode:UITextFieldViewModeAlways];
    _passwordText.leftView= [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Key-25.png"]];
    
    
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    
    [activityIndicatorView setCenter:self.view.center];
    
    [self.view addSubview:activityIndicatorView];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    //[textField setHidden:YES];
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}
/*!
 *@brief when keyboard appear reduce the view y value to appears in top
 */
- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.view setFrame:CGRectMake(0,-90,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}
/*!
 *@brief when keyboard disappear  view wiil be normal
 */
-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}
/*!
 *@brief send username and password by post method. when success server will return success message with userid. Userid is stored in nsuserdefaults.
 */

-(void)sendcredientials
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/LoginApi/",ipaddress]];
    
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSString * params =[NSString stringWithFormat:@"Email=%@&Password=%@",_usernameText.text,_passwordText.text];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if(error == nil)
                                          {
                                              NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                              NSLog(@"^Data = %@",text);
                                              dispatch_async(dispatch_get_main_queue(),
                                                             ^{
                                                                 
                                                                 NSArray *values=[text componentsSeparatedByString:@"_"];
                                                                 
                                                                 if([[values objectAtIndex:0] isEqualToString:@"\"successuser"])
                                                                 {
                                                                     [[NSUserDefaults standardUserDefaults] setObject:[values objectAtIndex:1] forKey:@"userid"];
                                                                     [[NSUserDefaults standardUserDefaults]synchronize];
                                                                     
                                                                     
                                                                     NSString *str=[[NSUserDefaults standardUserDefaults]stringForKey:@"userid"];
                                                                     NSLog(@"%@",str);
                                                                     [self performSegueWithIdentifier:@"loginview" sender:self];
                                                                     [activityIndicatorView stopAnimating];
                                                                     _submitButton.hidden=NO;
                                                                     
                                                                 }
                                                                 
                                                                 else
                                                                 {
                                                                     SCLAlertView *alert = [[SCLAlertView alloc] init];
                                                                     
                                                                     [alert showError:self title:@"Sorry"
                                                                             subTitle:@"Username or Password incorrect"
                                                                     closeButtonTitle:@"OK" duration:0.0f];
                                                                     [activityIndicatorView stopAnimating];
                                                                     _submitButton.hidden=NO;
                                                                     
                                                                     
                                                                 }
                                                             });
                                          }
                                          else
                                          {
                                              
                                              
                                              SCLAlertView *alert = [[SCLAlertView alloc] init];
                                              
                                              [alert showWarning:self title:@"Oops" subTitle:@"Plese check your connection" closeButtonTitle:@"Ok" duration:0.0f];
                                              [activityIndicatorView stopAnimating];
                                              _submitButton.hidden=NO;
                                              
                                          }
                                          
                                      }];
    [dataTask resume];
    
}
/*!
 *@brief When login success it wii redirect to main homepage.
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"loginview"]) {
        [segue destinationViewController];
    }
    
}
- (IBAction)submitButton:(id)sender {
    [activityIndicatorView startAnimating];
    _submitButton.hidden=YES;
    [self sendcredientials];
}

@end

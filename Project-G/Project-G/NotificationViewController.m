//
//  NotificationViewController.m
//  Project-G
//
//  Created by Sabari on 16/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "NotificationViewController.h"
#import "DGActivityIndicatorView.h"
#import "Urlclass.h"
@interface NotificationViewController ()
{
    NSString *ipaddress;
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    DGActivityIndicatorView *activityIndicatorView;
    
}

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    self.navigationItem.title=@"Notification";
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    [activityIndicatorView setCenter:self.view.center];
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    [self getnotification];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getnotification
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetNotifications/0/100",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            
                                                            [[self navigationController] tabBarItem].badgeValue =nil;
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               
                                                                               [_notificationtable reloadData];
                                                                               [activityIndicatorView stopAnimating];
                                                                           });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dictResponse count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    UILabel *post=(UILabel *)[cell viewWithTag:1];
    UILabel *time=(UILabel *)[cell viewWithTag:2];
    post.text=[NSString stringWithFormat:@"%@ %@",[[dictResponse valueForKey:@"UserName"]objectAtIndex:indexPath.row],[[dictResponse valueForKey:@"Message"]objectAtIndex:indexPath.row]];
    NSString *str = [NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"PostedDate"]objectAtIndex:indexPath.row]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
    NSDate *currentDate = [dateFormatter dateFromString:str];
    time.text = [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
    
    
    return cell;
}
/*!
 *@brief stringForTimeIntervalSinceCreated will convert timestamp valu to time like(1 day ago,2 minitutes ago).
 */
- (NSString *)stringForTimeIntervalSinceCreated:(NSDate *)dateTime serverTime:(NSDate *)serverDateTime{
    NSInteger MinInterval;
    NSInteger HourInterval;
    NSInteger DayInterval;
    NSInteger DayModules;
    
    NSInteger interval = labs((NSInteger)[dateTime timeIntervalSinceDate:serverDateTime]);
    if(interval >= 86400)
    {
        DayInterval  = interval/86400;
        DayModules = interval%86400;
        if(DayModules!=0)
        {
            if(DayModules>=3600){
                //HourInterval=DayModules/3600;
                return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
            }
            else {
                if(DayModules>=60){
                    //MinInterval=DayModules/60;
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
                else {
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
            }
        }
        else
        {
            return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
        }
        
    }
    
    else{
        
        if(interval>=3600)
        {
            
            HourInterval= interval/3600;
            return [NSString stringWithFormat:@"%li hours ago", (long)HourInterval];
            
        }
        
        else if(interval>=60){
            
            MinInterval = interval/60;
            
            return [NSString stringWithFormat:@"%li minutes ago", (long)MinInterval];
        }
        else{
            return [NSString stringWithFormat:@"%li Sec ago", (long)interval];
        }
        
    }
    
}

@end

//
//  ViewController.m
//  Project-G
//
//  Created by Sabari on 16/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ViewController.h"
#import "HMSegmentedControl.h"
#import "ChatViewController.h"
#import "Urlclass.h"
@interface ViewController ()
{
    NSString *ipaddress;
    int userid;
    ChatViewController *cht;
    NSDictionary *dictResponse,*dictresponse1;
}
@property (strong, nonatomic) IBOutlet UIView *Problemview;
@property (strong, nonatomic) IBOutlet UIView *componentview;
@property (strong, nonatomic) IBOutlet UIView *activityview;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentValueChanged;
- (IBAction)segmentValueChanged:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *segview;

@end
@implementation ViewController
@synthesize chat1,chat,hubConnection,onlinememlist,touserid,onlinegroupname,onlinegroupid,val;
- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    [self getnotification];
    self.Problemview.hidden = NO;
    self.componentview.hidden = YES;
    self.activityview.hidden = YES;
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);
    NSString *userid1=[[NSUserDefaults standardUserDefaults]stringForKey:@"userid"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notification" object:self];
    userid=[userid1 intValue];
    [self getloginguserdetail];
    
    _onlinusercon=[[NSMutableArray alloc]init];
    onlinememlist=[[NSMutableArray alloc]init];
    touserid=[[NSMutableArray alloc]init];
    val=[[NSMutableArray alloc]init];
    onlinegroupid=[[NSMutableArray alloc]init];
    onlinegroupname=[[NSMutableArray alloc]init];
    cht=[[ChatViewController alloc]init];
    [val addObject:[NSNumber numberWithInt:userid]];
    
    hubConnection = [SRHubConnection connectionWithURLString:[NSString stringWithFormat:@"%@",ipaddress]];
    chat1 = [hubConnection createHubProxy:@"chatHub"];
    
    [hubConnection setStarted:^{
        NSLog(@"^ Connection Started");
        
        
    }];
    [hubConnection setReceived:^(NSArray *message) {
        
        NSLog(@"^Connection Recieved Data: %@",message);
    }];
    [hubConnection setConnectionSlow:^{
        NSLog(@"^Connection Slow");
    }];
    [hubConnection setReconnecting:^{
        NSLog(@"^Connection Reconnecting");
    }];
    [hubConnection setReconnected:^{
        NSLog(@"^Connection Reconnected");
    }];
    [hubConnection setClosed:^{
        NSLog(@"^Connection Closed");
    }];
    [hubConnection setError:^(NSError *error) {
        NSLog(@"^Connection Error %@",error);
    }];
    // Start the connection
    __weak typeof(self) weakSelf = self;
    // __weak ViewController *self_ = self;
    hubConnection.started = ^{
        
        [weakSelf.chat1 invoke:@"Connect" withArgs:nil];
        [weakSelf.chat1 invoke:@"GetOnlineUser" withArgs:nil];
    };
    [hubConnection start];
    //UpdatepublicComment
    [chat1 on:@"SendConnectedUsersList" perform:self selector:@selector(SendConnectedUsersList:)];
    [chat1 on:@"SendNotificationToGroup" perform:self selector:@selector(SendNotificationToGroup: valueid:)];
    [chat1 on:@"SendPostToPublic" perform:self selector:@selector(SendPostToPublic: valueid:)];
    [chat1 on:@"UpdatepublicComment" perform:self selector:@selector(UpdatepublicComment: valueid:)];
    [chat1 on:@"SendSolutionToGroup" perform:self selector:@selector(SendSolutionToGroup: valueid:)];
    [chat1 on:@"NotificationForLike" perform:self selector:@selector(NotificationForLike: valueid1: valueid2:)];
    [chat1 on:@"NotificationForUnLike" perform:self selector:@selector(NotificationForLike: valueid1: valueid2:)];
    [chat1 on:@"updateUser" perform:self selector:@selector(updateUser:)];
    
    //SendConnectedUsersList
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendmessage:)
                                                 name:@"sendmessage"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendgroupmessage:)
                                                 name:@"sendgroupmessage"
                                               object:nil];
    
    
    HMSegmentedControl *segmentedControl3 = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Solution", @"Activity Feed"]];
    [segmentedControl3 setFrame:CGRectMake(0,0,viewWidth,43)];
    [segmentedControl3 setIndexChangeBlock:^(NSInteger index) {
        NSLog(@"Selected index %ld (via block)", (long)index);
        switch (index) {
            case 0:
                self.Problemview.hidden = NO;
                self.activityview.hidden = YES;
                break;
            case 1:
                self.Problemview.hidden = YES;
                self.activityview.hidden = NO;
                break;
                
            default:
                break;
        }
        
    }];
    segmentedControl3.selectionIndicatorHeight = 4.0f;
    segmentedControl3.backgroundColor =[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:0.5];
    segmentedControl3.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor]};
    segmentedControl3.selectionIndicatorColor = [UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1];
    segmentedControl3.selectionIndicatorBoxColor = [UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1];
    segmentedControl3.selectionIndicatorBoxOpacity = 1.0;
    segmentedControl3.selectedTitleTextAttributes=@{NSForegroundColorAttributeName : [UIColor whiteColor]};
    segmentedControl3.selectionStyle = HMSegmentedControlSelectionStyleBox;
    segmentedControl3.selectedSegmentIndex = 0;
    segmentedControl3.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl3.shouldAnimateUserSelection = NO;
    segmentedControl3.tag = 2;
    [self.segview addSubview:segmentedControl3];
    
    // Do any additional setup after loading the view, typically from a nib.
}
/*!
 *@brief update connected new user as online in caht page from signalr and strore in NSUserdefaults
 */
-(void)updateUser:(NSArray *)details
{
    [onlinememlist addObject:[details valueForKey:@"UserName"]];
    [_onlinusercon addObject:[details valueForKey:@"ConnectionId"]];
    [touserid addObject:[details valueForKey:@"UserId"]];
    NSArray *copy = [onlinememlist copy];
    
    NSInteger index = [copy count] - 1;
    
    for (id object in [copy reverseObjectEnumerator])
    {
        
        if ([onlinememlist indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound)
            
        {
            [onlinememlist removeObjectAtIndex:index];
            [_onlinusercon removeObjectAtIndex:index];
            [touserid removeObjectAtIndex:index];
            
        }
        index--;
    }
    [[NSUserDefaults standardUserDefaults] setObject:onlinememlist forKey:@"memberlist"];
    [[NSUserDefaults standardUserDefaults] setObject:_onlinusercon forKey:@"userconid"];
    [[NSUserDefaults standardUserDefaults] setObject:touserid forKey:@"touserid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateLeftTable" object:self];
    
}
/*!
 *@brief get connected user from signalr and strore in NSUserdefaults
 */
-(void)SendConnectedUsersList:(NSArray *)userlist
{
    
    
    for(int i=0;i<userlist.count;i++)
    {
        
        [onlinememlist addObject:[[userlist valueForKey:@"UserName"]objectAtIndex:i]];
        [_onlinusercon addObject:[[userlist valueForKey:@"ConnectionId"]objectAtIndex:i]];
        [touserid addObject:[[userlist valueForKey:@"UserId"]objectAtIndex:i]];
        NSLog(@"%@",[[userlist valueForKey:@"UserName"]objectAtIndex:i]);
        NSLog(@"%@",[[userlist valueForKey:@"ConnectionId"]objectAtIndex:i]);
        NSLog(@"%@",[[userlist valueForKey:@"UserId"]objectAtIndex:i]);
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:onlinememlist forKey:@"memberlist"];
    [[NSUserDefaults standardUserDefaults] setObject:_onlinusercon forKey:@"userconid"];
    [[NSUserDefaults standardUserDefaults] setObject:touserid forKey:@"touserid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

/*!
 *@brief get notification for post promblem from signalr and send NSNotification to problem pages to upadate problem data.
 */
-(void)SendNotificationToGroup:(NSArray *)details valueid:(NSString *)value
{
    //    int uservalue=[value intValue];
    //    if(uservalue!=userid)
    //    {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"postproblem" object:self];
    // }
    
}
/*!
 *@brief get notification for Activity post from signalr and send NSNotification to post pages to upadate Activity Feed data.
 */
-(void)SendPostToPublic:(NSArray *)details valueid:(NSString *)value
{
    int uservalue=[value intValue];
    if(uservalue!=userid)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"activityfeed" object:self];
        [self getnotification];
    }
    
}
/*!
 *@brief get notification for comments from signalr and send NSNotification to post pages to upadate comment data.
 */
-(void)UpdatepublicComment:(NSDictionary *)details valueid:(NSString *)value
{
    int uservalue=[value intValue];
    if(uservalue!=userid)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"publicComment" object:self  userInfo:details];
        [self getnotification];
    }
    
}
/*!
 *@brief get notification for solution  promblem from signalr and send NSNotification to problem pages to upadate solution data.
 */
-(void)SendSolutionToGroup:(NSDictionary *)details valueid:(NSString *)value
{
    int uservalue=[value intValue];
    if(uservalue!=userid)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SendSolutionToGroup" object:self userInfo:details];
        [self getnotification];
    }
    
}
/*!
 *@brief handle like from singnalr and send NSNotification to all post pages.
 */
-(void)NotificationForLike:(NSArray *)details valueid1:(NSString *)value valueid2:(NSString *)value1
{
    int uservalue=[value intValue];
    if(uservalue!=userid)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationForLike" object:self];
        [self getnotification];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief get notification for logged user and update the tab bar count in notification page.
 */
-(void) getnotification
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetNotifications/0/30",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            NSString *tabcount=[NSString stringWithFormat:@"%lu",(unsigned long)dictResponse.count];
                                                            if(dictResponse.count==0)
                                                            {
                                                                [[super.tabBarController.viewControllers objectAtIndex:3] tabBarItem].badgeValue = nil;
                                                            }
                                                            else
                                                            {
                                                                [[super.tabBarController.viewControllers objectAtIndex:3] tabBarItem].badgeValue = tabcount;
                                                            }
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
/*!
 *@brief get current logged user name and saved in NSUserDefaults.
 */
-(void)getloginguserdetail
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetProfileDetails/%d",ipaddress,userid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                       {
                                           if(error == nil)
                                           {
                                               dictresponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                               [[NSUserDefaults standardUserDefaults] setObject:[dictresponse1 valueForKey:@"UserName"] forKey:@"currentusername"];
                                               [[NSUserDefaults standardUserDefaults]synchronize];
                                           }
                                       }];
    
    [dataTask resume];
    
}
/*!
 *@brief get private message send from app through nsnotification and invoke sendprivatemessage when click send in chat controller
 */
- (void)sendmessage:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"sendmessage"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *messagetext=[[NSUserDefaults standardUserDefaults]valueForKey:@"chatmessage"];;
            [chat1 invoke:@"SendPrivateMessage" withArgs:messagetext];
            
        });
        
    }
}

/*!
 *@brief get group message send from app through nsnotification and invoke sendServerMessageForGroups when click send in chat controller
 */
- (void)sendgroupmessage:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"sendgroupmessage"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *groupmessagetext=[[NSUserDefaults standardUserDefaults]valueForKey:@"groupchatmessage"];;
            [chat1 invoke:@"sendServerMessageForGroups" withArgs:groupmessagetext];
            
        });
        
    }
}
/*!
 *@brief get group message send from app through nsnotification and invoke sendServerMessageForGroups when click send in chat controller
 */
- (IBAction)segmentValueChanged:(id)sender {
    UISegmentedControl *segment = sender;
    
    switch (segment.selectedSegmentIndex) {
        case 0:
            self.Problemview.hidden = NO;
            self.activityview.hidden = YES;
            break;
        case 2:
            self.Problemview.hidden = YES;
            self.activityview.hidden = NO;
            break;
            
        default:
            break;
    }
    
}
-(void) viewWillAppear:(BOOL)animated{
    [chat1 on:@"onConnected" perform:self selector:@selector(onConnected: userName: ConnectedUsers: Name: Userid:)];
    [chat1 on:@"onNewUserConnected" perform:self selector:@selector(onNewUserConnected: userName: Name: Userid:)];
    [chat1 on:@"OnlineConnect" perform:self selector:@selector(OnlineConnect:)];
    [chat1 on:@"Group" perform:self selector:@selector(Group: groupname:)];
    [chat1 on:@"setDetails" perform:self selector:@selector(setDetails: Fullname:)];
    [chat1 on:@"onUserDisconnected" perform:self selector:@selector(onUserDisconnected: userName: Userid:)];
    [chat1 on:@"sendnotification" perform:self selector:@selector(sendnotification:User1:User2:Message:DateHelper:FrmName:fromid:)];
    [chat1 on:@"sendPrivateMessage" perform:self selector:@selector(sendPrivateMessage:fromUsername:Message:Tousername:DateHelper:FrmName:Toid:)];
    [chat1 on:@"GroupMessage" perform:self selector:@selector(GroupMessage:Message:groupid:Gname:DateHelper:)];
}
/*!
 *@brief get connected user from signalr and strore in NSUserdefaults
 */
- (void)onConnected:(id)message userName:(NSString *)user_name  ConnectedUsers:(NSArray *)userlist Name:(NSString *)user_name1 Userid:(int)user_id
{
    for(int i=0;i<userlist.count-1;i++)
    {
        
        [onlinememlist addObject:[[userlist valueForKey:@"UserName"]objectAtIndex:i]];
        [_onlinusercon addObject:[[userlist valueForKey:@"ConnectionId"]objectAtIndex:i]];
        [touserid addObject:[[userlist valueForKey:@"UserId"]objectAtIndex:i]];
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:onlinememlist forKey:@"memberlist"];
    [[NSUserDefaults standardUserDefaults] setObject:_onlinusercon forKey:@"userconid"];
    [[NSUserDefaults standardUserDefaults] setObject:touserid forKey:@"touserid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
    
    
}
/*!
 *@brief store connected user as online in caht page from signalr and strore in NSUserdefaults
 */
- (void)onNewUserConnected:(id)message userName:(NSString *)user_name  Name:(NSString *)user_name1 Userid:(id)user_id
{
    [onlinememlist addObject:user_name];
    [_onlinusercon addObject:message];
    [touserid addObject:user_id];
    NSArray *copy = [onlinememlist copy];
    
    NSInteger index = [copy count] - 1;
    
    for (id object in [copy reverseObjectEnumerator])
    {
        
        if ([onlinememlist indexOfObject:object inRange:NSMakeRange(0, index)] != NSNotFound)
            
        {
            [onlinememlist removeObjectAtIndex:index];
            [_onlinusercon removeObjectAtIndex:index];
            [touserid removeObjectAtIndex:index];
            
        }
        index--;
    }
    [[NSUserDefaults standardUserDefaults] setObject:onlinememlist forKey:@"memberlist"];
    [[NSUserDefaults standardUserDefaults] setObject:_onlinusercon forKey:@"userconid"];
    [[NSUserDefaults standardUserDefaults] setObject:touserid forKey:@"touserid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateLeftTable" object:self];
    
    
}
/*!
 *@brief get online user countupdateLeftTable
 */
- (void)OnlineConnect:(id)usercount
{
    
    NSLog(@"%@",usercount);
    
}
/*!
 *@brief remove disconnected user and update in chat page from signalr and strore in NSUserdefaults
 */
- (void)onUserDisconnected:(id)value1 userName:(NSString *)user_name Userid:(id)user_id
{
    [onlinememlist removeObject:user_name];
    [_onlinusercon removeObject:value1];
    [touserid removeObject:user_id];
    [[NSUserDefaults standardUserDefaults] setObject:onlinememlist forKey:@"memberlist"];
    [[NSUserDefaults standardUserDefaults] setObject:_onlinusercon forKey:@"userconid"];
    [[NSUserDefaults standardUserDefaults] setObject:touserid forKey:@"touserid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    // [usrlist userlist];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateLeftTable" object:self];
    
}
/*!
 *@brief get group name and group id from signalr and store in nsuserdefaults to access in chat controller
 */

-(void)Group:(id)data groupname:(NSString *)Groupname
{
    
    [onlinegroupname addObject:Groupname];
    [onlinegroupid addObject:data];
    [[NSUserDefaults standardUserDefaults] setObject:onlinegroupname forKey:@"onlinegroupname"];
    [[NSUserDefaults standardUserDefaults] setObject:onlinegroupid forKey:@"onlinegroupid"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    
}
/*!
 *@brief get currentuser from signalr
 */
-(void)setDetails:(NSString *)username Fullname:(NSString *)fullname

{
    [[NSUserDefaults standardUserDefaults] setValue:fullname forKey:@"Currentuser"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}
/*!
 *@brief send private message ans details user from signalr and send back to chat view controller to display
 */

- (void)sendPrivateMessage:(id)connectionid fromUsername:(NSString *)fromusername  Message:(NSString *)message Tousername:(NSString *)tousername DateHelper:(NSString *)date FrmName:(NSString *)frm_user Toid:(id)to_id
{
    if([frm_user isEqualToString:[[NSUserDefaults standardUserDefaults]stringForKey:@"Currentuser"]])
    {
        [cht sendPrivateMessage:connectionid fromUsername:fromusername Message:message Tousername:tousername DateHelper:date FrmName:frm_user Toid:to_id];
    }
    else
    {
        //        [TSMessage showNotificationWithTitle:[NSString stringWithFormat:@"Message from %@",frm_user]subtitle:message type:TSMessageNotificationTypeSuccess];
        [cht sendPrivateMessage:connectionid fromUsername:fromusername Message:message Tousername:tousername DateHelper:date FrmName:frm_user Toid:to_id];
    }
    
    
    
}
/*!
 *@brief get message notification from signalr and strore in NSUserdefaults
 */
- (void)sendnotification:(id)connectionid User1:(NSString *)username1  User2:(NSString *)username2 Message:(NSString *)msg DateHelper:(NSString *)date FrmName:(NSString *)frm_user fromid:(id)frm_id
{
    
}
/*!
 *@brief send group message and details user from signalr and send back to chat view controller to display
 */

-(void)GroupMessage:(NSString *)uname Message:(NSString *)groupMessage groupid:(NSString*)Groupid Gname:(NSString *)gname DateHelper:(NSString *)date
{
    if([uname isEqualToString:[[NSUserDefaults standardUserDefaults]stringForKey:@"Currentuser"]])
    {
        [cht GroupMessage:uname Message:groupMessage groupid:Groupid Gname:gname DateHelper:date];
    }
    else
    {
        //        [TSMessage showNotificationWithTitle:[NSString stringWithFormat:@"Message from %@ to %@",uname,gname]
        //                                    subtitle:groupMessage
        //                                        type:TSMessageNotificationTypeSuccess];
        [cht GroupMessage:uname Message:groupMessage groupid:Groupid Gname:gname DateHelper:date];
    }
    
    
}
@end

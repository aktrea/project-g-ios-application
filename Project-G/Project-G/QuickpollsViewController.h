//
//  QuickpollsViewController.h
//  Project-G
//
//  Created by Sabari on 25/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickpollsViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITableView *polltable;
@property (strong, nonatomic) IBOutlet UITextField *grouptextfield;
@property (strong, nonatomic) IBOutlet UIView *createpollview;
- (IBAction)createpoll:(id)sender;

@end

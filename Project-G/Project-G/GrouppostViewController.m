//
//  GrouppostViewController.m
//  Project-G
//
//  Created by Sabari on 8/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "GrouppostViewController.h"
#import "GroupStatustextTableViewCell.h"
#import "GroupStatusimageTableViewCell.h"
#import "GroupProblemTableViewCell.h"
#import "GroupStatusViewController.h"
#import "Urlclass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ViewImageVC.h"
#import "SolutionViewController.h"
#import "CommentViewController.h"
@interface GrouppostViewController ()
{
    
    NSString *ipaddress,*groupid;
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    GroupStatusimageTableViewCell *cell;
    GroupStatustextTableViewCell *cell1;
    GroupProblemTableViewCell *cell2;
    NSString *status;
    bool likestate;
    int scrollvalue;
}

@end

@implementation GrouppostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    likestate=false;
    groupid=[[NSUserDefaults standardUserDefaults]stringForKey:@"groupid"];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    scrollvalue=10;
    [self getstatus];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postproblem:)
                                                 name:@"postproblem"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postproblem:)
                                                 name:@"NotificationForLike"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SendSolutionToGroup:)name:@"SendSolutionToGroup" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postproblem:)
                                                 name:@"publicComment"
                                               object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getstatus];
}
/*!
 *@brief when other user post,like,comment this method invoke by NSNotification
 */
- (void)postproblem:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"postproblem"]|[[notif name] isEqualToString:@"NotificationForLike"]||[[notif name] isEqualToString:@"publicComment"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getstatus];
        });
        
    }
}
/*!
 *@brief when other user post solution this method invoke by NSNotification
 */
-(void)SendSolutionToGroup:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"SendSolutionToGroup"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getstatus];
        });
        
    }
}
/*!
 *@brief getgrouppost by pass intial and end value. dictresponce will get data and assign to tableview to display post.
 */
-(void) getgrouppost:(int)intial :(int)end
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/post/getgrouppost/%@/%d/%d",ipaddress,groupid,intial,end];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            likestate=false;
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               
                                                                               if([dictResponse isKindOfClass:[NSString class]])
                                                                               {
                                                                                   
                                                                               }
                                                                               else
                                                                               {
                                                                                   [_eventtable reloadData];
                                                                               }
                                                                           });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return [dictResponse count];
    }
    else
    {
        return 0;
    }
    
}
/*!
 *@brief cell is for normal post, cell1 is form image post, cell2 is for problem post. Based on document list normal post and image post will be display. Based on status type problem post will be display
 */
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"activitycell";
    static NSString *cellidentifier1=@"activitycellplain";
    static NSString *cellidentifier2=@"cell";
    cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    cell1=[tableView dequeueReusableCellWithIdentifier:cellidentifier1];
    cell2=[tableView dequeueReusableCellWithIdentifier:cellidentifier2];
    if(cell2==nil)
    {
        cell2=[[GroupProblemTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier2];
    }
    if(cell==nil)
    {
        cell=[[GroupStatusimageTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    if(cell1==nil)
    {
        cell1=[[GroupStatustextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier1];
    }
    NSInteger statustype=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostType"]objectAtIndex:indexPath.row] integerValue];
    if(statustype==0)
    {
        cell1.postbyname.text=[NSString stringWithFormat:@"%@ Posted a Status",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"]objectAtIndex:indexPath.row]];
        if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row]==[NSNull null])
        {
            cell1.eventlabel.text=@"";
        }
        else
        {
            cell1.eventlabel.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row];
        }
        NSString *str = [NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]objectAtIndex:indexPath.row]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        int moduratorvalue=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"IsModerator"]objectAtIndex:indexPath.row]intValue];
        if(moduratorvalue==1)
        {
            cell1.starimage.hidden=NO;
        }
        else
        {
            cell1.starimage.hidden=YES;
        }
        
        cell1.postedtime.text = [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        cell1.postcontent.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"]objectAtIndex:indexPath.row];
        cell1.likelabel.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:indexPath.row]];
        NSArray *commentcount=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"]objectAtIndex:indexPath.row];
        cell1.commentlabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]];
        cell1.commentbut.tag=indexPath.row;
        cell1.likebut.tag=indexPath.row;
        [cell1.likebut addTarget:self action:@selector(like: ) forControlEvents:UIControlEventTouchUpInside];
        [cell1.commentbut addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        NSString *like=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]objectAtIndex:indexPath.row]];
        if([like isEqualToString:@"1"])
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell1.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        }
        else
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell1.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            
        }
        
        return cell1;
        
        
    }
    else if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row] == [NSNull null] )
    {
        cell2.postedby.text=[NSString stringWithFormat:@"%@ Posted a problem",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"]objectAtIndex:indexPath.row]];
        NSString *str = [NSString stringWithFormat:@"%@", [[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]objectAtIndex:indexPath.row]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        NSArray *solution;
        int moduratorvalue=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"IsModerator"]objectAtIndex:indexPath.row]intValue];
        if(moduratorvalue==1)
        {
            cell2.starimage.hidden=NO;
        }
        else
        {
            cell2.starimage.hidden=YES;
        }
        
        cell2.postedtime.text= [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row]==[NSNull null])
        {
            cell2.eventlabel.text=@"";
        }
        else
        {
            cell2.eventlabel.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row];
        }
        cell2.porblemtext.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"]objectAtIndex:indexPath.row]];
        if([[[dictResponse valueForKey:@"Problemandsolution"]objectAtIndex:indexPath.row] valueForKey:@"Solution"] == [NSNull null] )
        {
            solution=nil;
            
        }
        else
        {
            solution=[[[dictResponse valueForKey:@"Problemandsolution"]objectAtIndex:indexPath.row] valueForKey:@"Solution"];
        }
        
        cell2.solutionbut.tag=indexPath.row;
        [cell2.solutionbut setTitle:[NSString stringWithFormat:@"%lu Solution",(unsigned long)solution.count] forState: UIControlStateNormal];
        [[cell2.solutionbut layer] setBorderWidth:1.0f];
        [[cell2.solutionbut layer] setBorderColor:[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1].CGColor];
        
        [cell2.solutionbut addTarget:self action:@selector(solution:) forControlEvents:UIControlEventTouchUpInside];
        cell2.likebut.tag=indexPath.row;
        [cell2.likebut addTarget:self action:@selector(like: ) forControlEvents:UIControlEventTouchUpInside];
        cell2.likelabel.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:indexPath.row]];
        cell2.backgroundColor=[UIColor clearColor];
        NSString *like=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]objectAtIndex:indexPath.row]];
        if([like isEqualToString:@"1"])
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell2.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        }
        else
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell2.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            
        }
        return cell2;
    }
    else
    {
        NSArray *document=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row];
        NSString *doc=[[document valueForKey:@"documentlink"]objectAtIndex:0];
        NSString *url_Img1 = [NSString stringWithFormat:@"%@%@",ipaddress,doc];
        NSString *url_Img2 = [[document valueForKey:@"documentname"]objectAtIndex:0];
        
        NSString *url_Img_FULL = [url_Img1 stringByAppendingPathComponent:url_Img2];
        
        
        cell.imgindicator.hidden=YES;
        cell.postimage.tag = indexPath.row;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                              action:@selector(imageTap:)];
        [cell.postimage addGestureRecognizer:tap];
        
        //Enable the image to be clicked
        cell.postimage.userInteractionEnabled = YES;
        
        [cell.postimage sd_setImageWithURL:[NSURL URLWithString:url_Img_FULL]
                          placeholderImage:[UIImage imageNamed:@"Ok Filled-100-3.png"]
                                   options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
        cell.postimage.clipsToBounds = YES;
        
        cell.postedbyname.text=[NSString stringWithFormat:@"%@ Posted a Status",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"]objectAtIndex:indexPath.row]];
        if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row]==[NSNull null])
        {
            cell.eventlabel.text=@"";
        }
        else
        {
            cell.eventlabel.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:indexPath.row];
        }
        int moduratorvalue=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"IsModerator"]objectAtIndex:indexPath.row]intValue];
        if(moduratorvalue==1)
        {
            cell.starimage.hidden=NO;
        }
        else
        {
            cell.starimage.hidden=YES;
        }
        
        NSString *str = [NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]objectAtIndex:indexPath.row]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        cell.postedtime.text = [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        cell.posttext.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"]objectAtIndex:indexPath.row];
        cell.likebut.tag=indexPath.row;
        [cell.likebut addTarget:self action:@selector(like: ) forControlEvents:UIControlEventTouchUpInside];
        NSString *like=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]objectAtIndex:indexPath.row]];
        if([like isEqualToString:@"1"])
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        }
        else
        {
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            
        }
        cell.commentbut.tag=indexPath.row;
        [cell.commentbut addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        cell.likelabel.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:indexPath.row]];
        NSArray *commentcount=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"CommentsList"]objectAtIndex:indexPath.row];
        cell.commentlabel.text=[NSString stringWithFormat:@"%lu",(unsigned long)[commentcount count]];
        
        return cell;
    }
}
/*!
 *@brief imageTap execute based on UITapGestureRecognizer
 */
- (void)imageTap:(UITapGestureRecognizer *)recognizer
{
    NSArray *document=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:recognizer.view.tag];
    NSString *doc=[[document valueForKey:@"documentlink"]objectAtIndex:0];
    NSString *url_Img1 = [NSString stringWithFormat:@"%@%@",ipaddress,doc];
    NSString *url_Img2 = [[document valueForKey:@"documentname"]objectAtIndex:0];
    
    NSString *url_Img_FULL = [url_Img1 stringByAppendingPathComponent:url_Img2];
    
    [self performSegueWithIdentifier:@"imageview" sender:url_Img_FULL];
}
/*!
 *@brief handle height for image post and normal post,problem solution post.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostType"]objectAtIndex:indexPath.row]==0)
    {
        return 188 ;
    }
    else if([[[dictResponse valueForKey:@"Posts"]valueForKey:@"documents"]objectAtIndex:indexPath.row] == [NSNull null] )
    {
        return 175 ;
    }
    else
    {
        return 369;
    }
}
/*!
 *@brief stringForTimeIntervalSinceCreated will convert timestamp valu to time like(1 day ago,2 minitutes ago).
 */
- (NSString *)stringForTimeIntervalSinceCreated:(NSDate *)dateTime serverTime:(NSDate *)serverDateTime{
    NSInteger MinInterval;
    NSInteger HourInterval;
    NSInteger DayInterval;
    NSInteger DayModules;
    
    NSInteger interval = labs((NSInteger)[dateTime timeIntervalSinceDate:serverDateTime]);
    if(interval >= 86400)
    {
        DayInterval  = interval/86400;
        DayModules = interval%86400;
        if(DayModules!=0)
        {
            if(DayModules>=3600){
                //HourInterval=DayModules/3600;
                return [NSString stringWithFormat:@"%li days", (long)DayInterval];
            }
            else {
                if(DayModules>=60){
                    //MinInterval=DayModules/60;
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
                else {
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
            }
        }
        else
        {
            return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
        }
        
    }
    
    else{
        
        if(interval>=3600)
        {
            
            HourInterval= interval/3600;
            return [NSString stringWithFormat:@"%li hours ago", (long)HourInterval];
            
        }
        
        else if(interval>=60){
            
            MinInterval = interval/60;
            
            return [NSString stringWithFormat:@"%li minutes ago", (long)MinInterval];
        }
        else{
            return [NSString stringWithFormat:@"%li Sec ago", (long)interval];
        }
        
    }
    
}
/*!
 *@brief comment will execute when user select comment. Get data based on selection and redirect to comment page
 */
-(void) comment:(id)sender
{
    NSArray *getdataarray=(NSArray *)dictResponse;
    NSDictionary *getactivity=[getdataarray objectAtIndex:[sender tag]];
    [self performSegueWithIdentifier:@"groupcomment" sender:getactivity];
}
/*!
 *@brief solution will execute when user select solution. Get data based on selection and redirect to solution page
 */
-(void) solution:(id)sender
{
    NSArray *getdataarray=(NSArray *)dictResponse;
    NSDictionary *getproblem=[getdataarray objectAtIndex:[sender tag]];
    [self performSegueWithIdentifier:@"groupsolution" sender:getproblem];
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"groupstatus"]) {
        GroupStatusViewController *stgroup=[segue destinationViewController];
        stgroup.groupid=groupid.copy;
    }
    else if ([[segue identifier] isEqualToString:@"groupsolution"]) {
        SolutionViewController *solutionsegue= [segue destinationViewController];
        solutionsegue.Solutiondata=[sender copy];
        
    }
    
    else if ([[segue identifier] isEqualToString:@"groupcomment"])
    {
        CommentViewController *commentsegue= [segue destinationViewController];
        commentsegue.activitydata=[sender copy];
    }
    else if ([[segue identifier] isEqualToString:@"imageview"])
    {
        ViewImageVC *img=[segue destinationViewController];
        img.imagedata=[sender copy];
        
    }
    
}
/*!
 *@brief handle like and unlike. like and unlike change based on image and send postid to server for like and unlike. like will update based on server response.
 */
-(IBAction)like:(UIButton *)sender
{
    NSInteger row=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    int likecount;
    likecount=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:sender.tag] intValue];
    cell =[_eventtable cellForRowAtIndexPath:indexPath];
    if ([[cell.likebut backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"Like1.png"]])
    {
        if(likestate==false)
        {
            likestate=true;
            likecount--;
            if(likecount<0)
            {
                likecount=0;
            }
            
            cell.likelabel.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/unlike?postId=%@",ipaddress,[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]objectAtIndex:sender.tag]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       [self getgrouppost:0 :scrollvalue];
                                                                       // [activityIndicatorView startAnimating];
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
        }
        NSLog(@"Same");
    }
    else
    {
        if(likestate==false)
        {
            likestate=true;
            likecount++;
            cell.likelabel.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/savelike?postId=%@",ipaddress,[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]objectAtIndex:sender.tag]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       [self getgrouppost:0 :scrollvalue];
                                                                       //  [activityIndicatorView startAnimating];
                                                                       
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
        }
        NSLog(@"Not Same");
    }
}
/*!
 *@brief redirection to status viewcontroller base on identifier. when user select the status box.
 */
- (IBAction)Navigatebut:(id)sender {
    [self performSegueWithIdentifier:@"groupstatus" sender:self];
}
/*!
 *@brief get status from server using NSURLSession.if user joined post will be display call([self geteventpost:0 :scrollvalue]). if not post will be hide.
 */
-(void)getstatus
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/event/status?groupId=%@",ipaddress,groupid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            status=[NSString stringWithFormat:@"%@",dictresponse3];
                                                            if([status isEqualToString:@"Joined"])
                                                            {
                                                                [self getgrouppost:0 :scrollvalue];
                                                            }
                                                            else
                                                            {
                                                                _statusview.hidden=YES;
                                                            }
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}
/*!
 *@brief get last index from table and call getpost method to get the group post increment by 10
 */

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [dictResponse count]-1 ) {
        scrollvalue=scrollvalue+10;
        [self getgrouppost:0 :scrollvalue];
    }
}
/*!
 *@brief add activity indicator at the bottom of the cell when add new group post
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    NSArray *visibleRows = [_eventtable visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [_eventtable indexPathForCell:lastVisibleCell];
    if(path.row == [dictResponse count]-1)
    {
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        [spinner startAnimating];
        spinner.frame = CGRectMake(self.view.frame.size.width/2, 0, 0, 44);
        [headerView addSubview:spinner];
    }
    
    return headerView;
}

@end

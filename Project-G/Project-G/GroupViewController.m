//
//  GroupViewController.m
//  Project-G
//
//  Created by Sabari on 7/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "GroupViewController.h"
#import "GroupTableViewCell.h"
#import "GroupdiscussionViewController.h"
#import "SCLAlertView.h"
#import "Urlclass.h"
@interface GroupViewController ()
{
    NSString *ipaddress;
    NSDictionary *dictResponse, *dictResponse1,*dictresponse2;
}

@end

@implementation GroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    [self getgroupdata];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief get table details from server using NSURLSession and display in table.
 */
-(void)getgroupdata
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/agendaevent/GetGroupDiscussionData?Id=3",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               
                                                                               [_grouplisttable reloadData];
                                                                           });
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dictResponse count];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *events=[[dictResponse valueForKey:@"SubEvents"] objectAtIndex:0];
    NSLog(@"%lu",(unsigned long)events.count);
    return events.count;
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier1 = @"section";
    GroupTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    UILabel *topic=(UILabel *)[headerView viewWithTag:1];
    UILabel *day=(UILabel *)[headerView viewWithTag:2];
    UILabel *date=(UILabel *)[headerView viewWithTag:3];
    topic.text=[[dictResponse valueForKey:@"Topic"]objectAtIndex:section];
    day.text=[NSString stringWithFormat:@"Day %@",[[dictResponse valueForKey:@"Day"]objectAtIndex:section]];
    date.text=[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"DateTime"]objectAtIndex:section]];
    return headerView;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell ==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    UILabel *topicname=(UILabel *)[cell viewWithTag:1];
    UILabel *speaker=(UILabel *)[cell viewWithTag:2];
    UILabel *participantlist=(UILabel *)[cell viewWithTag:3];
    NSArray *subevents=[[dictResponse valueForKey:@"SubEvents"] objectAtIndex:0];
    topicname.text=[[subevents valueForKey:@"TopicName"] objectAtIndex:indexPath.row];
    speaker.text=[[subevents valueForKey:@"TableLeaderName"] objectAtIndex:indexPath.row];
    participantlist.text=[NSString stringWithFormat:@"%@/%@",[[subevents valueForKey:@"JoinedUsersCount"] objectAtIndex:indexPath.row],[[subevents valueForKey:@"UserLimit"] objectAtIndex:indexPath.row]];
    
    return cell;
}
/*!
 *@brief User can go to group if already joined or if table is full alert message wil display.If table participant count is <10 user go to group view and join the group.
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *subevents=[[dictResponse valueForKey:@"SubEvents"] objectAtIndex:0];
    //groupdiscussion
    int ismember=[[[subevents valueForKey:@"IsMember"]objectAtIndex:indexPath.row]intValue];
    int tablestatus=[[[subevents valueForKey:@"IsTableFull"]objectAtIndex:indexPath.row]intValue];
    if(ismember==1)
    {
        [self performSegueWithIdentifier:@"groupdiscussion" sender:[[subevents valueForKey:@"GroupId"]objectAtIndex:indexPath.row]];
        [[NSUserDefaults standardUserDefaults] setObject:[[subevents valueForKey:@"GroupId"]objectAtIndex:indexPath.row] forKey:@"groupid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    else if(tablestatus==1)
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert showWarning:self title:@"Sorry" subTitle:@"Table full" closeButtonTitle:@"Ok" duration:0.0f];
    }
    else
    {
        [self performSegueWithIdentifier:@"groupdiscussion" sender:[[subevents valueForKey:@"GroupId"]objectAtIndex:indexPath.row]];
        [[NSUserDefaults standardUserDefaults] setObject:[[subevents valueForKey:@"GroupId"]objectAtIndex:indexPath.row] forKey:@"groupid"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString: @"groupdiscussion"])
    {
        GroupdiscussionViewController *grp=[segue destinationViewController];
        grp.groupid=[sender copy];
        
        
    }
}


@end

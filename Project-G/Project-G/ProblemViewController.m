//
//  ProblemViewController.m
//  Project-G
//
//  Created by Sabari on 16/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ProblemViewController.h"
#import "CZPicker.h"
#import "ProblemTableViewCell.h"
#import "ProblemtextTableViewCell.h"
#import "SolutionViewController.h"
#import "SCLAlertView.h"
#import "DGActivityIndicatorView.h"
#import "Urlclass.h"
#import "GetdataApi.h"
@interface ProblemViewController ()<CZPickerViewDataSource, CZPickerViewDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    NSString *ipaddress;
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    NSInteger rows1;
    int pickervalue;
    CZPickerView *eventpick,*countrypick;
    ProblemtextTableViewCell *cell;
    bool likestate;
    NSString *postprob;
    int val,scrollvalue;
    DGActivityIndicatorView *activityIndicatorView;
}
@property CZPickerView *pickerWithImage;
@end

@implementation ProblemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    scrollvalue=10;
    // ipaddress=@"192.168.3.116/akton";
    self.navigationItem.title=@"Solution Space";
    activityIndicatorView = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeDoubleBounce tintColor:[UIColor colorWithRed:0.000 green:0.251 blue:0.502 alpha:1.000] size:20.0f];
    [activityIndicatorView setCenter:self.view.center];
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    
    [[_solutionpublish_but layer] setBorderWidth:1.0f];
    [[_solutionpublish_but layer] setBorderColor:[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1].CGColor];
    [[_postcontent_text layer] setBorderWidth:0.50f];
    [[_postcontent_text layer] setBorderColor:[UIColor colorWithRed:204.0f/255 green:204.0f/255 blue:204.0f/255 alpha:1].CGColor];
    _postcontent_text.layer.cornerRadius=4;
    _postcontent_text.layer.masksToBounds=YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postproblem:)
                                                 name:@"postproblem"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(postproblem:)
                                                 name:@"NotificationForLike"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SendSolutionToGroup:)name:@"SendSolutionToGroup" object:nil];
    
    
    likestate=false;
    [self getpost:0 :scrollvalue];
    [self getgroups];
    
    _event_textfield.delegate=self;
    _country_textfield.delegate=self;
    _postcontent_text.delegate=self;
    countrypick = [[CZPickerView alloc] initWithHeaderTitle:@"Select Group" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Select"];
    eventpick = [[CZPickerView alloc] initWithHeaderTitle:@"Select Group" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Select"];
    
    // Do any additional setup after loading the view.
}
/*!
 *@brief when other user post problem,like this method invoke by NSNotification
 */
- (void)postproblem:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"postproblem"]||[[notif name] isEqualToString:@"NotificationForLike"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getpost:0 :scrollvalue];
        });
        
    }
}
/*!
 *@brief when other user post solution this method invoke by NSNotification
 */
-(void)SendSolutionToGroup:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"SendSolutionToGroup"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getpost:0 :scrollvalue];
        });
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getpost:0 :scrollvalue];
    [self getgroups];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief getproblempost by pass intial and end value. dictresponce will get data and assign to tableview to display post.
 */
-(void) getpost:(int)intial :(int)end
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetProblemandSolution/%d/%d",ipaddress,intial,end];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            likestate=false;
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];                                                            dispatch_async(dispatch_get_main_queue(),^{
                                                                [_problemtable reloadData];
                                                                [activityIndicatorView stopAnimating];
                                                            });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
-(void)getlocation
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/Problem/GetLocation",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}
/*!
 *@brief get groups from server using NSURLSession
 */
-(void)getgroups
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/user/groups",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dictResponse count];
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
/*!
 *@brief display posted user,time in uitableview section
 */
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"section";
    ProblemTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    headerView.postedby.text=[NSString stringWithFormat:@"%@ Posted a problem",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostBy"]objectAtIndex:section]];
    int moduratorvalue=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"IsModerator"]objectAtIndex:section]intValue];
    if(moduratorvalue==1)
    {
        headerView.starimage.hidden=NO;
    }
    else
    {
        headerView.starimage.hidden=YES;
    }
    
    NSString *str = [NSString stringWithFormat:@"%@", [[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]objectAtIndex:section]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
    NSDate *currentDate = [dateFormatter dateFromString:str];
    
    headerView.postedtime.text= [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
    headerView.eventlabel.text=[[[dictResponse valueForKey:@"Posts"]valueForKey:@"EventName"]objectAtIndex:section];
    // convert to date
    
    //    headerView.postedtime.text=[self dateDiff:[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostedDate"]objectAtIndex:section]];
    return headerView;
}
/*!
 *@brief adjust cell height based on problem content
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = [[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"]objectAtIndex:indexPath.section];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    CGRect textRect = [str boundingRectWithSize:CGSizeMake(280,999)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:@{NSParagraphStyleAttributeName: paragraphStyle.copy}
                                        context:nil];
    CGSize size= textRect.size;
    return size.height +110;
}
/*!
 *@brief display problem data,posted by,time, solution count and like.
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[ProblemtextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    cell.porblemtext.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"PostContent"]objectAtIndex:indexPath.section]];
    NSArray *solution=[[[dictResponse valueForKey:@"Problemandsolution"]objectAtIndex:indexPath.section] valueForKey:@"Solution"];
    cell.likecount.text=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:indexPath.section]];
    cell.solutionbut.tag=indexPath.section;
    [cell.solutionbut setTitle:[NSString stringWithFormat:@"%lu Solution",(unsigned long)solution.count] forState: UIControlStateNormal];
    [[cell.solutionbut layer] setBorderWidth:1.0f];
    [[cell.solutionbut layer] setBorderColor:[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1].CGColor];
    
    [cell.solutionbut addTarget:self action:@selector(solution:) forControlEvents:UIControlEventTouchUpInside];
    cell.likebut.tag=indexPath.section;
    //cell.likecount.tag=indexPath.section*1000+indexPath.row;
    [cell.likebut addTarget:self action:@selector(like: ) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundColor=[UIColor clearColor];
    NSString *like=[NSString stringWithFormat:@"%@",[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Isliked"]objectAtIndex:indexPath.section]];
    if([like isEqualToString:@"1"])
    {
        UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
        [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
    }
    else
    {
        UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
        [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
        
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 77;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView

{
    textView.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if([_postcontent_text.text isEqualToString:@"Post the problem"])
    {
        _postcontent_text.text = @"";
    }
    _postcontent_text.textColor = [UIColor blackColor];
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [self.postcontent_text endEditing:YES];
    return YES;
}
- (BOOL) textView: (UITextView*) textView
shouldChangeTextInRange: (NSRange) range
  replacementText: (NSString*) text
{
    
    _postcontent_text.textColor = [UIColor blackColor];
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidChange:(UITextView *)textView{
    if([_postcontent_text.text isEqualToString:@"Post the problem"]||[_postcontent_text.text isEqualToString:@"Post the proble"])
    {
        _postcontent_text.text = @"";
    }
    _postcontent_text.textColor = [UIColor blackColor];
    
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [self.view endEditing:YES];
    return YES;
}
/*!
 *@brief call getgroups to show group name when select textfield.
 */
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField==_event_textfield)
    {
        pickervalue=0;
        eventpick.delegate = self;
        eventpick.dataSource = self;
        eventpick.needFooterView = YES;
        [self getgroups];
        [eventpick show];
        return NO;
    }
    else if(textField==_country_textfield)
    {
        pickervalue=1;
        countrypick.delegate = self;
        countrypick.dataSource = self;
        countrypick.needFooterView = YES;
        [countrypick show];
        return NO;
    }
    else{
        return NO;
    }
}
/*!
 *@brief show group name in czpickerview
 */

- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:[dictresponse3 valueForKey:@"GroupName"][row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    if(pickervalue==0)
    {
        return [dictresponse3 valueForKey:@"GroupName"] [row];
    }
    else if(pickervalue==1)
    {
        return [dictResponse1 valueForKey:@"GeographicName"] [row];
    }
    return 0;
}

- (UIImage *)czpickerView:(CZPickerView *)pickerView imageForRow:(NSInteger)row {
    if([pickerView isEqual:self.pickerWithImage]) {
        if(pickervalue==0)
        {
            return [dictresponse3 valueForKey:@"GroupName"] [row];
        }
        else if(pickervalue==1)
        {
            return [dictResponse1 valueForKey:@"GeographicName"] [row];
        }
    }
    return nil;
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    if(pickervalue==0)
    {
        return [[dictresponse3 valueForKey:@"GroupName"] count];
    }
    else if(pickervalue==1)
    {
        return [[dictResponse1 valueForKey:@"GeographicName"] count];
    }
    return 0;
}
/*!
 *@brief get group name usinf CZPickerView
 */
- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row {
    if(pickervalue==0)
    {
        _event_textfield.text=[dictresponse3 valueForKey:@"GroupName"] [row];
    }
    else if(pickervalue==1)
    {
        _country_textfield.text=[dictResponse1 valueForKey:@"GeographicName"] [row];
    }
    
    
    
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows {
    if(pickervalue==0)
    {
        
        for (NSNumber *n in rows) {
            rows1 = [n integerValue];
        }
    }
    else if(pickervalue==1)
    {
        for (NSNumber *n in rows) {
            rows1 = [n integerValue];
        }
        
    }
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    [self.navigationController setNavigationBarHidden:NO];
    NSLog(@"^Canceled.");
}
/*!
 *@brief solution will execute when user select solution. Get data based on selection and redirect to solution page
 */
-(void) solution:(id)sender
{
    NSArray *getdataarray=(NSArray *)dictResponse;
    NSDictionary *getproblem=[getdataarray objectAtIndex:[sender tag]];
    [self performSegueWithIdentifier:@"solution" sender:getproblem];
}
/*!
 *@brief handle like and unlike. like and unlike change based on image and send postid to server for like and unlike. like will update based on server response.
 */
-(IBAction)like:(UIButton *)sender
{
    NSInteger section=[sender tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:section];
    int likecount;
    likecount=[[[[dictResponse valueForKey:@"Posts"]valueForKey:@"likes"]objectAtIndex:sender.tag] intValue];
    cell = [_problemtable cellForRowAtIndexPath:indexPath];
    if ([[cell.likebut backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"Like1.png"]])
    {
        if(likestate==false)
        {
            likestate=true;
            likecount--;
            if(likecount<0)
            {
                likecount=0;
            }
            cell.likecount.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like2.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/unlike?postId=%@",ipaddress,[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]objectAtIndex:sender.tag]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       NSDictionary *dit=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                                       
                                                                       [self getpost:0 :scrollvalue];
                                                                       // [activityIndicatorView startAnimating];
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
            
            
        }
        NSLog(@"Same image");
        
    }else{
        if(likestate==false)
        {
            likestate=true;
            likecount++;
            cell.likecount.text =[NSString stringWithFormat:@"%d",likecount];
            UIImage *myCheese = [UIImage imageNamed:@"Like1.png"];
            [cell.likebut setBackgroundImage:myCheese forState:UIControlStateNormal];
            NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/post/savelike?postId=%@",ipaddress,[[[dictResponse valueForKey:@"Posts"]valueForKey:@"Id"]objectAtIndex:sender.tag]]];
            
            NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
            
            [urlRequest setHTTPMethod:@"POST"];
            
            
            
            NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                   if(error == nil)
                                                                   {
                                                                       NSDictionary *dit=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                                       NSLog(@"%@",dit);
                                                                       [self getpost:0 :scrollvalue];
                                                                       //  [activityIndicatorView startAnimating];
                                                                       
                                                                   }
                                                                   else{
                                                                       NSLog(@"^%@",[error localizedDescription]);
                                                                   }
                                                                   
                                                               }];
            
            [dataTask resume];
        }
        NSLog(@"not Same image");
    }
    
}
/*!
 *@brief redirection to other viewcontroller base on identifier
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"solution"]) {
        SolutionViewController *solutionsegue= [segue destinationViewController];
        //solutionsegue.value=[sender copy];
        solutionsegue.Solutiondata=[sender copy];
        
    }
    
}
/*!
 *@brief stringForTimeIntervalSinceCreated will convert timestamp valu to time like(1 day ago,2 minitutes ago).
 */
- (NSString *)stringForTimeIntervalSinceCreated:(NSDate *)dateTime serverTime:(NSDate *)serverDateTime{
    NSInteger MinInterval;
    NSInteger HourInterval;
    NSInteger DayInterval;
    NSInteger DayModules;
    
    NSInteger interval = labs((NSInteger)[dateTime timeIntervalSinceDate:serverDateTime]);
    if(interval >= 86400)
    {
        DayInterval  = interval/86400;
        DayModules = interval%86400;
        if(DayModules!=0)
        {
            if(DayModules>=3600){
                //HourInterval=DayModules/3600;
                return [NSString stringWithFormat:@"%li days", (long)DayInterval];
            }
            else {
                if(DayModules>=60){
                    //MinInterval=DayModules/60;
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
                else {
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
            }
        }
        else
        {
            return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
        }
        
    }
    
    else{
        
        if(interval>=3600)
        {
            
            HourInterval= interval/3600;
            return [NSString stringWithFormat:@"%li hours ago", (long)HourInterval];
            
        }
        
        else if(interval>=60){
            
            MinInterval = interval/60;
            
            return [NSString stringWithFormat:@"%li minutes ago", (long)MinInterval];
        }
        else{
            return [NSString stringWithFormat:@"%li Sec ago", (long)interval];
        }
        
    }
    
}
- (IBAction)solutionpublish_but:(id)sender {
    postprob=_postcontent_text.text;
    [self highlight];
    _postcontent_text.text = @"Post the problem";
    _postcontent_text.textColor = [UIColor lightGrayColor];
    [self postproblem];
}
-(void)highlight
{
    _solutionpublish_but.backgroundColor=[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1];
    [_solutionpublish_but setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}
/*!
 *@brief post problem, postdata(NSDictionary) in post method and update problem data based on responce.
 */
-(void)postproblem
{
    if(_event_textfield.text.length==0)
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert showError:self title:@"Oops"
                subTitle:@"Please select event to post" closeButtonTitle:@"OK" duration:0.0f];
        _solutionpublish_but.backgroundColor=[UIColor whiteColor];
        [_solutionpublish_but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    else if([postprob isEqualToString:@"Post the problem"])
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert showError:self title:@"Oops"
                subTitle:@"Please type your problem" closeButtonTitle:@"OK" duration:0.0f];
        _solutionpublish_but.backgroundColor=[UIColor whiteColor];
        [_solutionpublish_but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    else
    {
        NSDictionary *postdata = [NSDictionary dictionaryWithObjectsAndKeys:[[dictresponse3 valueForKey:@"GroupId"]objectAtIndex:rows1],@"GroupId",postprob,@"PostContent",@"",@"GeographicId",nil];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSError *ss;
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/Problem/PostProblemForGroup",ipaddress]];
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postdata options:NSJSONWritingPrettyPrinted error:&ss];
        
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        
        [urlRequest setHTTPBody:jsonData];
        NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                               if(error == nil)
                                                               {
                                                                   //  NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                                   
                                                                   [self getpost:0 :scrollvalue];
                                                                   _solutionpublish_but.backgroundColor=[UIColor whiteColor];
                                                                   [_solutionpublish_but setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];                                                    }
                                                               else{
                                                                   NSLog(@"Error %@",[error localizedDescription]);
                                                               }
                                                               
                                                           }];
        
        [dataTask resume];
        
        
        
    }
    
}
/*!
 *@brief get last index from table and call getpost method to get the problem feed increment by 10
 */

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == [dictResponse count]-1 ) {
        scrollvalue=scrollvalue+10;
        [self getpost:0 :scrollvalue];
    }
}
/*!
 *@brief add activity indicator at the bottom of the cell when add new problem feed
 */
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    
    [headerView setBackgroundColor:[UIColor clearColor]];
    if (section == [dictResponse count]-1 )
    {
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
        [spinner startAnimating];
        spinner.frame = CGRectMake(self.view.frame.size.width/2, 0, 0, 44);
        [headerView addSubview:spinner];
    }
    
    
    
    return headerView;
    
}
@end

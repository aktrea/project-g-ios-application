//
//  CommentViewController.h
//  Project-G
//
//  Created by Sabari on 24/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property (strong, nonatomic) IBOutlet UITableView *commenttable;
@property (strong, nonatomic) IBOutlet UILabel *postbyname;
@property (strong, nonatomic) IBOutlet UILabel *posttime;
@property (strong, nonatomic) IBOutlet UILabel *postcontent;
@property (strong, nonatomic) IBOutlet UIImageView *postimage;
@property (strong, nonatomic) IBOutlet UILabel *likelabel;
@property (strong, nonatomic) IBOutlet UIButton *likebut;
@property (strong, nonatomic) IBOutlet UILabel *commentlabel;
- (IBAction)likebut:(id)sender;
- (IBAction)commentbut:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *comment;
@property (strong, nonatomic) IBOutlet UIView *commentview;

@property id value;
@property NSMutableDictionary *activitydata;
@end

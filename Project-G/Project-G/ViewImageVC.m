//
//  ViewImageVC.m
//  ProjectV2
//
//  Created by Sabari on 30/6/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ViewImageVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ViewImageVC ()
{
    UIPinchGestureRecognizer *twoFingerPinch;
    UIImageView *img;
}

@end

@implementation ViewImageVC

- (void)viewDidLoad {
    [super viewDidLoad];
    /*!
     *@brief get image url from user touch and displayed in imageview using SDWebImage
     */
    img = [[UIImageView alloc] init];
    [img sd_setImageWithURL:[NSURL URLWithString:_imagedata]
           placeholderImage:[UIImage imageNamed:@"Ok Filled-100-3.png"]
                    options:SDWebImageProgressiveDownload];
    img.userInteractionEnabled = YES;
    img.backgroundColor = [UIColor clearColor];
    
    img.contentMode =  UIViewContentModeScaleAspectFit;
    img.frame = CGRectMake(50, 0, self.view.frame.size.width-100, self.view.frame.size.height-50);
    [self.view addSubview:img];
    /*!
     *@brief add zoom option to tha image using two finger to zoom in and zoom out
     */
    
    twoFingerPinch=[[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(twoFingerPinch:)];
    [img addGestureRecognizer:twoFingerPinch];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer
{
    /*!
     *@brief get value using recognizer to zoom in and zoom out
     */
    if (recognizer.scale >1.0f && recognizer.scale < 2.5f) {
        CGAffineTransform transform = CGAffineTransformMakeScale(recognizer.scale, recognizer.scale);
        img.transform = transform;
    }
    else if (recognizer.scale < 1.0f)
    {
        CGAffineTransform transform = CGAffineTransformMakeScale(recognizer.scale, recognizer.scale);
        img.transform = transform;
    }
}

@end

//
//  ViewController.h
//  Project-G
//
//  Created by Sabari on 16/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Signalr.h"

@interface ViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property(weak) SRHubProxy *chat,*chat1;
@property SRHubConnection *hubConnection;
@property(nonatomic, retain) NSMutableArray *onlinememlist;
@property(nonatomic, retain) NSMutableArray *touserid;
@property NSMutableArray *onlinusercon;
@property(nonatomic, retain) NSMutableArray *onlinegroupname;
@property(nonatomic, retain) NSMutableArray *onlinegroupid;
@property NSMutableArray *val;

@end


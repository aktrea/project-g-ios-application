//
//  solutiontextTableViewCell.h
//  Project-G
//
//  Created by Sabari on 18/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface solutiontextTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UILabel *problemtext;
@property (strong, nonatomic) IBOutlet UIButton *postbut;
@property (strong, nonatomic) IBOutlet UITextField *textsolution;
@property (strong, nonatomic) IBOutlet UITextView *textsolution1;

@end

//
//  GroupStatusViewController.h
//  Project-G
//
//  Created by Sabari on 8/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupStatusViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate>

- (IBAction)addphoto:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *statustextmessage;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UIImageView *userimage;
@property (strong, nonatomic) IBOutlet UIButton *Publish_but;
- (IBAction)Publish_but:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *imageview;
@property (strong, nonatomic) IBOutlet UITextView *imagedectext;
@property (strong, nonatomic) IBOutlet UIImageView *imagepost;
- (IBAction)status:(id)sender;
- (IBAction)problem:(id)sender;
@property NSString *groupid;
@end

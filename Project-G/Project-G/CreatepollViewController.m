//
//  CreatepollViewController.m
//  Project-G
//
//  Created by Sabari on 29/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "CreatepollViewController.h"
#import "CZPicker.h"
#import "Urlclass.h"
#import "GetdataApi.h"

@interface CreatepollViewController ()<CZPickerViewDataSource, CZPickerViewDelegate,UITextFieldDelegate,UITextViewDelegate>

{
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    int optioncount;
    CZPickerView *eventpick;
     NSInteger rows1,selectrow;
    NSString *ipaddress,*groupid;
    NSMutableArray *ansvalue;
}

@end

@implementation CreatepollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.hidden=NO;
    optioncount=2;
    ansvalue=[[NSMutableArray alloc]init];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    [self getgroups];
    _grouptext.delegate=self;
    _questiontext.delegate=self;
    [[_questiontext layer] setBorderWidth:0.50f];
    [[_questiontext layer] setBorderColor:[UIColor colorWithRed:204.0f/255 green:204.0f/255 blue:204.0f/255 alpha:1].CGColor];
    _questiontext.layer.cornerRadius=4;
    _questiontext.layer.masksToBounds=YES;
      eventpick = [[CZPickerView alloc] initWithHeaderTitle:@"Select Group" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Select"];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.view.hidden=NO;
    [self getgroups];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getgroups
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/user/groups",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return optioncount;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    UITextField *txt=(UITextField *)[cell viewWithTag:1];
    txt.delegate=self;
    if(ansvalue.count!=0)
    {
        if(indexPath.row<ansvalue.count)
        {
        txt.text=[ansvalue objectAtIndex:indexPath.row];
        }
        else
        {
            txt.text=@"";
        }
    }
    else
    {
        txt.text=@"";
    }
        
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row>1)
    {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       --optioncount;
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           if([ansvalue count]<=indexPath.row)
                           {
                              [_polltable reloadData];
                           }
                           else
                           {
                           [ansvalue removeObjectAtIndex:indexPath.row];
                               [_polltable reloadData];
                           }
                       });
    }
    }
}
- (IBAction)add:(id)sender {
    [self.view endEditing:YES];
    ++optioncount;
    dispatch_async(dispatch_get_main_queue(),
                   ^{
    [_polltable reloadData];
    [self performSelector:@selector(goToBottom) withObject:nil afterDelay:0.2];
                   });
}

- (IBAction)submitcreate:(id)sender {
    [self submitcreatepoll];
}
-(void) textFieldDidEndEditing: (UITextField * ) textField
{
    if(textField == _grouptext)
    {
        
    }
    else
    {
        [ansvalue addObject:textField.text];
        
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [self.view endEditing:YES];
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if(textField == _grouptext)
    {
    eventpick.delegate = self;
    eventpick.dataSource = self;
    eventpick.needFooterView = YES;
        [self getgroups];
    [eventpick show];
         [self.view endEditing:YES];
    return NO;
    }
    else
    {
        if([textField.text isEqualToString:@""])
        {
            [self.view setFrame:CGRectMake(0,-120,self.view.frame.size.width,self.view.frame.size.height)];
        }
        else
        {
            [self.view setFrame:CGRectMake(0,-120,self.view.frame.size.width,self.view.frame.size.height)];
        }
        return YES;
    }
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField == _grouptext)
    {
    eventpick.delegate = self;
    eventpick.dataSource = self;
    eventpick.needFooterView = YES;
    [eventpick show];
        [self.view endEditing:YES];
    return NO;
    }
    else
    {
        if([textField.text isEqualToString:@""])
        {
        
        }
        else
        {
        
        }

        return YES;
    }
    
}
/*!
 *@brief show group name in czpickerview
 */

- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:[dictresponse3 valueForKey:@"GroupName"][row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    
    return [dictresponse3 valueForKey:@"GroupName"] [row];
}
- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView {
    
    return [[dictresponse3 valueForKey:@"GroupName"] count];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row {
    
    _grouptext.text=[dictresponse3 valueForKey:@"GroupName"] [row];
    groupid=[dictresponse3 valueForKey:@"GroupId"] [row];
    [self getpolls:groupid];
    
    
}
- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemsAtRows:(NSArray *)rows {
    for (NSNumber *n in rows)
    {
        rows1 = [n integerValue];
    }
}
- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView {
    
    NSLog(@"^Canceled.");
}
-(void)submitcreatepoll
{
     if(ansvalue.count!=0)
     {
    NSMutableArray * arr = [[NSMutableArray alloc] init];
    for(int i=0;i<ansvalue.count;i++)
    {
        [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[ansvalue objectAtIndex:i],@"AnswerText",nil]];
    }
    NSDictionary *Answers1=[NSDictionary dictionaryWithObjectsAndKeys:@"0",@"ID",_questiontext.text,@"QuestionText",arr,@"Answers",@"0",@"QuestionType",@"0",@"SequenceNo",_challengeid,@"Challengeid",@"0",@"votes",groupid,@"groupid",nil];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSError *ss;
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/quickpoll/createpoll",ipaddress]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:Answers1 options:NSJSONWritingPrettyPrinted error:&ss];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [urlRequest setHTTPBody:jsonData];
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           if(error == nil)
                                                           { 
                                                               NSDictionary * response = dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                               NSLog(@"Responce for server%@",response);
                                                               [self.navigationController popViewControllerAnimated:YES];
                                                           }
                                                           else
                                                           {
                                                               NSLog(@"Error %@",[error localizedDescription]);
                                                           }
                                                           
                                                       }];
    
    [dataTask resume];
    
     }

}
-(void)getpolls:(NSString *)idvalue
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/quickpoll/getpollquestions?id=%@",ipaddress,idvalue];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            NSLog(@"%@",dictResponse1);
                                                            if(dictResponse1.count==0)
                                                            {
                                                               _challengeid=@"0";
                                                            }
                                                            else
                                                            {
                                                            _challengeid=[[dictResponse1 valueForKey:@"Challengeid"]objectAtIndex:0];
                                                            }
                                                           
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView

{
    textView.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if([_questiontext.text isEqualToString:@"Question"])
    {
        _questiontext.text = @"";
    }
    _questiontext.textColor = [UIColor blackColor];
    return YES;
}
- (BOOL) textView: (UITextView*) textView
shouldChangeTextInRange: (NSRange) range
  replacementText: (NSString*) text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if(textView == _questiontext)
    {
        [_questiontext endEditing:YES];
    }
    return YES;
}
-(void)tablescrollpoint
{
    NSInteger lastSectionIndex = MAX(0, [self.polltable numberOfSections] - 1);
    NSInteger lastRowIndex = MAX(0, [self.polltable numberOfRowsInSection:lastSectionIndex] - 1);
    
    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:lastRowIndex inSection:lastSectionIndex];
    [self.polltable scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    
}
-(void)goToBottom
{
    if(optioncount>3)
    {
    [self tablescrollpoint];
    }
}

@end

//
//  CreatepollViewController.h
//  Project-G
//
//  Created by Sabari on 29/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreatepollViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *polltable;
@property (strong, nonatomic) IBOutlet UITextField *grouptext;
@property (strong, nonatomic) IBOutlet UITextView *questiontext;
- (IBAction)add:(id)sender;
- (IBAction)submitcreate:(id)sender;
@property NSString *challengeid;
@end

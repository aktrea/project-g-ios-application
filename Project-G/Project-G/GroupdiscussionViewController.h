//
//  GroupdiscussionViewController.h
//  Project-G
//
//  Created by Sabari on 7/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupdiscussionViewController : UIViewController@property (strong, nonatomic) IBOutlet UIView *seg;
@property (strong, nonatomic) IBOutlet UIView *groupview;
@property (strong, nonatomic) IBOutlet UIView *post;
@property NSString * groupid;
@property (strong, nonatomic) IBOutlet UIButton *participantlist;
- (IBAction)participantlist:(id)sender;

@end

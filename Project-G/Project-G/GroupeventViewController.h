//
//  GroupeventViewController.h
//  Project-G
//
//  Created by Sabari on 7/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupeventViewController : UIViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UILabel *topicname;
@property (strong, nonatomic) IBOutlet UILabel *date;
- (IBAction)jointable:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *jointable;

@end

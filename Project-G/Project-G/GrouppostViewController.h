//
//  GrouppostViewController.h
//  Project-G
//
//  Created by Sabari on 8/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrouppostViewController : UIViewController
<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITextView *statustext;
- (IBAction)Navigatebut:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *statusview;
@property (strong, nonatomic) IBOutlet UITableView *eventtable;
@end

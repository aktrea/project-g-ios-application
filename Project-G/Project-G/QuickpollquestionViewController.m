//
//  QuickpollquestionViewController.m
//  Project-G
//
//  Created by Sabari on 25/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "QuickpollquestionViewController.h"
#import "MBCircularProgressBarView.h"
#import "PollanswerbarTableViewCell.h"
#import "PollcompleteanswerTableViewCell.h"
#import "Urlclass.h"
@interface QuickpollquestionViewController ()

{
    PollanswerbarTableViewCell *cell;
    NSString *ipaddress;
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
        NSInteger rows1,selectrow;
}

@end

@implementation QuickpollquestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    _completetable.hidden=YES;
    _questionlabel.text=[NSString stringWithFormat:@"%@",[_getpoll valueForKey:@"QuestionText"]];
    _questionpostname.text=[NSString stringWithFormat:@"%@",[_getpoll valueForKey:@"CreatedBy"]];
    for(int i=0;i<[[_getpoll valueForKey:@"Answers"] count];i++)
    {
        NSInteger select=[[[[_getpoll valueForKey:@"Answers"]valueForKey:@"IsSelected"]objectAtIndex:i]integerValue];
       if( select==1)
       {
           _questiontable.hidden=YES;
           _completetable.hidden=NO;
       }
    }
       // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getgroups
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/user/groups",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_completetable)
    {
        return [[_getpoll valueForKey:@"Answers"] count];
    }
    if(tableView == _questiontable)
    {
        return [[_getpoll valueForKey:@"Answers"] count]+1;
    }
    return 0;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _questiontable)
    {
    static NSString *cellidentifier=@"cell";
    static NSString *cellidentifier1=@"cell1";
UITableViewCell *cell1=[tableView dequeueReusableCellWithIdentifier:cellidentifier1];
    cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell1==nil)
    {
        cell1=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier1];
        
    }

    if(cell==nil)
    {
        cell=[[PollanswerbarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
        
        
    }
    NSInteger con=[[_getpoll valueForKey:@"Answers"] count]+1;
    if(indexPath.row+1==con)
    {
        UIButton *but=(UIButton *)[cell1 viewWithTag:1];
        [but addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
                return cell1;
       
    }
    else
    {

    UILabel *question=(UILabel *)[cell viewWithTag:1];
    question.text=[NSString stringWithFormat:@"%@",[[[_getpoll valueForKey:@"Answers"]valueForKey:@"AnswerText"]objectAtIndex:indexPath.row]];
        cell.option.text=[NSString stringWithFormat:@"%ld",indexPath.row+1];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    }
    if(tableView == _completetable)
    {
        static NSString *cellidentifier=@"cell";
        PollcompleteanswerTableViewCell *cell2=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
        if(cell2==nil)
        {
            cell2=[[PollcompleteanswerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
            
            
        }
        cell2.answerlabel.text=[NSString stringWithFormat:@"%@",[[[_getpoll valueForKey:@"Answers"]valueForKey:@"AnswerText"]objectAtIndex:indexPath.row]];
        cell2.votelabel.text=[NSString stringWithFormat:@"%@",[[[_getpoll valueForKey:@"Answers"]valueForKey:@"votes"]objectAtIndex:indexPath.row]];
        [UIView animateWithDuration:1.f animations:^{
            cell2.progress.value = [[[[_getpoll valueForKey:@"Answers"]valueForKey:@"Percentage"]objectAtIndex:indexPath.row] doubleValue];
        }];

return cell2;
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _questiontable)
    {
        NSInteger con=[[_getpoll valueForKey:@"Answers"] count]+1;
        if(indexPath.row+1==con)
        {
        }
        else
        {
            selectrow=indexPath.row;
     [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
   
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _questiontable)
    {
         NSInteger con=[[_getpoll valueForKey:@"Answers"] count]+1;
        if(indexPath.row+1==con)
        {
        }
        else
        {
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_questiontable)
    {
    NSInteger con=[[_getpoll valueForKey:@"Answers"] count];
    if(indexPath.row==con)
    {
        return 53;
    }
    else
    {
    return 73;
    }
    }
    if(tableView==_completetable)
    {
       return 72;
    }
    return 0;
}
-(void) submit:(id)sender
{
    
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSString *str=[NSString stringWithFormat:@"{\"ID\":\"%@\",\"ChallengeId\":\"%@\",\"QuestionId\":\"%@\",\"IsSelected\":\"true\"}&groupId=%d",[[[_getpoll valueForKey:@"Answers"]valueForKey:@"ID"]objectAtIndex:selectrow],[_getpoll valueForKey:@"Challengeid"],[[[_getpoll valueForKey:@"Answers"]valueForKey:@"QuestionId"]objectAtIndex:selectrow],[[_getpoll valueForKey:@"GroupId"] intValue]];
    NSString* str1 = [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    NSLog(@"%@",str1);
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/quickpoll/savepollanswer?ans=%@",ipaddress,str1]];
    NSLog(@"%@",url);
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    
    [urlRequest setHTTPMethod:@"POST"];
    
    
    
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           if(error == nil)
                                                           {
                                                               NSString *str=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                               NSLog(@"ssssss%@",str);
                                                               [self.navigationController popViewControllerAnimated:YES];
                                                           }
                                                           else{
                                                               NSLog(@"^%@",[error localizedDescription]);
                                                           }
                                                           
                                                       }];
    
    [dataTask resume];

 }
@end

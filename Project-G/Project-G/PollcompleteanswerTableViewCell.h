//
//  PollcompleteanswerTableViewCell.h
//  Project-G
//
//  Created by Sabari on 28/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarView.h"
@interface PollcompleteanswerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MBCircularProgressBarView *progress;
@property (strong, nonatomic) IBOutlet UILabel *answerlabel;
@property (strong, nonatomic) IBOutlet UILabel *votelabel;

@end

//
//  ActivityViewController.h
//  Project-G
//
//  Created by Sabari on 16/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITextView *statustext;
@property (strong, nonatomic) IBOutlet UIButton *navigatebut;
@property (strong, nonatomic) IBOutlet UITableView *activitytable;

- (IBAction)navigatebut:(id)sender;

@end

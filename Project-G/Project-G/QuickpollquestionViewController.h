//
//  QuickpollquestionViewController.h
//  Project-G
//
//  Created by Sabari on 25/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickpollquestionViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate>


@property (strong, nonatomic) IBOutlet UILabel *questionlabel;
@property (strong, nonatomic) IBOutlet UILabel *questionpostname;
@property (strong, nonatomic) NSDictionary *getpoll;
@property (strong, nonatomic) IBOutlet UITableView *questiontable;
@property (strong, nonatomic) IBOutlet UITableView *completetable;
@end

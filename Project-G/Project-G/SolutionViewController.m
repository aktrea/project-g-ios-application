//
//  SolutionViewController.m
//  Project-G
//
//  Created by Sabari on 18/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "SolutionViewController.h"
#import "SolutionTableViewCell.h"
#import "SolutionanswerTableViewCell.h"
#import "solutiontextTableViewCell.h"
#import "SCLAlertView.h"
#import "Urlclass.h"
#import "GetdataApi.h"
@interface SolutionViewController ()
{
    NSString *ipaddress;
    NSMutableDictionary *dictResponse, *dictResponse1,*dictresponse3,*getproblem,*solutionvalue1;;
    NSString *solutionValue;
    solutiontextTableViewCell *cell;
    NSArray *getcopy;
    int postmark;
    
    
}

@end

@implementation SolutionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    self.navigationItem.title=@"Solution";
    _solutiontable.hidden=NO;
    postmark=1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SendSolutionToGroup:)name:@"SendSolutionToGroup" object:nil];
    getproblem=_Solutiondata;
    NSLog(@"%@",getproblem);
    solutionvalue1=[getproblem valueForKey:@"Problemandsolution"];
    
    // Do any additional setup after loading the view.
}
/*!
 *@brief when other user post solution this method invoke by NSNotification
 */
-(void)SendSolutionToGroup:(NSNotification *)notif  {
    if ([[notif name] isEqualToString:@"SendSolutionToGroup"])
    {
        NSDictionary* solutiondata = notif.userInfo;
        NSMutableArray *sss=[[getproblem valueForKey:@"Problemandsolution"] valueForKey:@"Solution"];
        [sss addObject:solutiondata];
        [solutionvalue1 setObject:sss forKey:@"Solution"];
        dispatch_async(dispatch_get_main_queue(), ^{
            [_solutiontable reloadData];
        });
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger val=[[solutionvalue1 valueForKey:@"Solution"] count];
    return val+1;
}
/*!
 *@brief create section in table view to display the problem text,postedby,time
 */
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"section";
    SolutionTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    headerView.postedby.text=[NSString stringWithFormat:@"%@ Posted a problem",[[getproblem valueForKey:@"Posts"]valueForKey:@"PostBy"]];
    NSString *str = [NSString stringWithFormat:@"%@",[[getproblem valueForKey:@"Posts"]valueForKey:@"PostedDate"]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
    NSDate *currentDate = [dateFormatter dateFromString:str];
    headerView.postedtime.text= [self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
    
    
    return headerView;
}
/*!
 *@brief handle textfield and post buttopn in indexPath.row==0. Whwn user post solution solution(post solution method execute and get text from getText method). Remainning row will display solution posted by user.
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    static NSString *cellidentifier1=@"cell1";
    SolutionanswerTableViewCell *cell1=[tableView dequeueReusableCellWithIdentifier:cellidentifier1];
    
    if(cell==nil)
    {
        cell=[[solutiontextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    if(cell1==nil)
    {
        cell1=[[ SolutionanswerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier1];
        
    }
    if(indexPath.row==0)
    {
        cell.problemtext.text=[[getproblem valueForKey:@"Posts"]valueForKey:@"PostContent"];
        [[cell.postbut layer] setBorderWidth:1.0f];
        [[cell.postbut layer] setBorderColor:[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1].CGColor];
        [cell.postbut addTarget:self action:@selector(postsolution:) forControlEvents:UIControlEventTouchUpInside];
        cell.textsolution.delegate=self;
        cell.textsolution.tag=indexPath.row;
        cell.textsolution.text=@"";
        [cell.textsolution addTarget:self action:@selector(getText:) forControlEvents: UIControlEventEditingDidEnd];
        
        
        
        return cell;
    }
    else
    {
        cell1.postedby.text=[[[solutionvalue1 valueForKey:@"Solution"] valueForKey:@"SolutionBy"] objectAtIndex:indexPath.row-1];
        NSString *str = [NSString stringWithFormat:@"%@", [[[solutionvalue1 valueForKey:@"Solution"] valueForKey:@"PostedDateTime"] objectAtIndex:indexPath.row-1]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
        NSDate *currentDate = [dateFormatter dateFromString:str];
        cell1.postedtime.text=[self stringForTimeIntervalSinceCreated:[NSDate date] serverTime:currentDate];
        cell1.postedsolution.text=[[[solutionvalue1 valueForKey:@"Solution"] valueForKey:@"PostContent"] objectAtIndex:indexPath.row-1];
        return cell1;
    }
   }
/*!
 *@brief height of tableview section. In storyboard for section created as 77.
 */
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 77;
}
/*!
*@brief height of tableview row. Row height is automatically adjusted based on solution content.
*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        NSString *str = [[getproblem valueForKey:@"Posts"]valueForKey:@"PostContent"];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        CGRect textRect = [str boundingRectWithSize:CGSizeMake(280,999)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSParagraphStyleAttributeName: paragraphStyle.copy}
                                            context:nil];
        CGSize size= textRect.size;
        return size.height +110;
        
    }
    else
    {
        NSString *str = [[[solutionvalue1 valueForKey:@"Solution"] valueForKey:@"PostContent"] objectAtIndex:indexPath.row-1];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        CGRect textRect = [str boundingRectWithSize:CGSizeMake(280,999)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSParagraphStyleAttributeName: paragraphStyle.copy}
                                            context:nil];
        CGSize size= textRect.size;
        if(size.height>140)
        {
            return size.height +130;
        }
        else if(size.height>200)
        {
            return size.height+160;
        }
        else if(size.height>300)
        {
            return size.height+200;
        }
        else if(size.height>400)
        {
            return size.height+250;
        }
        else
        {
            return size.height +62;
        }
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [self.view endEditing:YES];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
    
}
/*!
 *@brief stringForTimeIntervalSinceCreated will convert timestamp valu to time like(1 day ago,2 minitutes ago).
 */
- (NSString *)stringForTimeIntervalSinceCreated:(NSDate *)dateTime serverTime:(NSDate *)serverDateTime{
    NSInteger MinInterval;
    NSInteger HourInterval;
    NSInteger DayInterval;
    NSInteger DayModules;
    
    NSInteger interval = labs((NSInteger)[dateTime timeIntervalSinceDate:serverDateTime]);
    if(interval >= 86400)
    {
        DayInterval  = interval/86400;
        DayModules = interval%86400;
        if(DayModules!=0)
        {
            if(DayModules>=3600){
                //HourInterval=DayModules/3600;
                return [NSString stringWithFormat:@"%li days", (long)DayInterval];
            }
            else {
                if(DayModules>=60){
                    //MinInterval=DayModules/60;
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
                else {
                    return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
                }
            }
        }
        else
        {
            return [NSString stringWithFormat:@"%li days ago", (long)DayInterval];
        }
        
    }
    
    else{
        
        if(interval>=3600)
        {
            
            HourInterval= interval/3600;
            return [NSString stringWithFormat:@"%li hours ago", (long)HourInterval];
            
        }
        
        else if(interval>=60){
            
            MinInterval = interval/60;
            
            return [NSString stringWithFormat:@"%li minutes ago", (long)MinInterval];
        }
        else{
            return [NSString stringWithFormat:@"minutes ago"];
        }
        
    }
    
}
/*!
 *@brief handle post solution by user.postmark==1(avoid duplicate post) will allow the post.If textfielf empty it will show alert.
 */

-(void) postsolution:(id)sender
{
    cell.postbut.userInteractionEnabled=NO;
    if(postmark==1)
    {
        postmark=0;
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
        [self.view endEditing:YES];
        if(solutionValue.length==0)
        {
            postmark=1;
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            
            SCLTextView *evenField = [alert addTextField:@"Enter your Solution"];
            evenField.keyboardType = UIKeyboardTypeDefault;
            
            
            [alert addButton:@"Post" validationBlock:^BOOL{
                if (evenField.text.length == 0)
                {
                    UIAlertController * alert=   [UIAlertController
                                                  alertControllerWithTitle:@"Sorry"
                                                  message:@"Please enter the Solution"
                                                  preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* okbutton = [UIAlertAction
                                               actionWithTitle:@"Ok"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action)
                                               {
                                                   
                                                   
                                               }];
                    
                    
                    [alert addAction:okbutton];
                    
                    
                    [self presentViewController:alert animated:YES completion:nil];
                    
                    [evenField becomeFirstResponder];
                    postmark=1;
                    return NO;
                }
                
                
                
                return YES;
            } actionBlock:^{
                solutionValue=evenField.text;
                [self postsolution_method];
            }];
            
            [alert showEdit:self title:@"Post Solution" subTitle:@"You can type your solution here" closeButtonTitle:@"Cancel" duration:0];
            
        }
        else{
            //cell.postbut.userInteractionEnabled=NO;
            // [activityIndicatorView startAnimating];
            [self postsolution_method];
            
        }
    }
}
/*!
 *@brief get text from the tableview(indexpath.row ==0)
 */
-(void) getText:(id)sender
{
    UITextField* textField =  (UITextField *)sender;
    solutionValue=textField.text;
    cell.textsolution.text=@"";
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
}
/*!
 *@brief post solution post solutiondata in post method and update solution data based on responce.
 */
-(void)postsolution_method
{
    NSDictionary *solutiondata = [NSDictionary dictionaryWithObjectsAndKeys:solutionValue,@"PostContent",[[getproblem valueForKey:@"Problemandsolution"]  valueForKey:@"ProblemId"],@"ProblemId",[getproblem valueForKey:@"PostType"],@"PostType",[[getproblem valueForKey:@"Problemandsolution"] valueForKey:@"PostId"],@"PostId",nil];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSError *ss;
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/Problem/PostSolution",ipaddress]];
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:solutiondata options:NSJSONWritingPrettyPrinted error:&ss];
    
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
    
    [urlRequest setHTTPBody:jsonData];
    NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                       completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                           if(error == nil)
                                                           {
                                                               postmark=1;
                                                               cell.postbut.userInteractionEnabled=YES;
                                                               NSDictionary * response = dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                               dispatch_async(dispatch_get_main_queue(),
                                                                              ^{
                                                                                  NSMutableArray *sss=[[getproblem valueForKey:@"Problemandsolution"] valueForKey:@"Solution"];
                                                                                  [sss addObject:response];
                                                                                  [solutionvalue1 setObject:sss forKey:@"Solution"];
                                                                                  [_solutiontable reloadData];
                                                                                  cell.textsolution.text=@"";
                                                                                  solutionValue=cell.textsolution.text;
                                                                              });
                                                           }
                                                           else{
                                                               NSLog(@"Error %@",[error localizedDescription]);
                                                           }
                                                           
                                                       }];
    
    [dataTask resume];
    
    
    
    
    
}

@end

//
//  ProblemTableViewCell.h
//  Project-G
//
//  Created by Sabari on 17/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProblemTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *postedby;
@property (strong, nonatomic) IBOutlet UILabel *postedtime;
@property (strong, nonatomic) IBOutlet UIImageView *postimage;
@property (strong, nonatomic) IBOutlet UIImageView *starimage;
@property (strong, nonatomic) IBOutlet UILabel *eventlabel;
@end

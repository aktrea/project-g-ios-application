//
//  PollanswerbarTableViewCell.h
//  Project-G
//
//  Created by Sabari on 28/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarView.h"

@interface PollanswerbarTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *question;
@property (strong, nonatomic) IBOutlet MBCircularProgressBarView *percentanswer;
@property (strong, nonatomic) IBOutlet UILabel *option;

@end

//
//  PollanswerbarTableViewCell.m
//  Project-G
//
//  Created by Sabari on 28/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "PollanswerbarTableViewCell.h"

@implementation PollanswerbarTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    [self.percentanswer setUnitString:@""];
    [self.percentanswer setShowValueString:NO];
    [self.percentanswer setProgressStrokeColor:[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1]];
    [self.percentanswer setProgressColor:[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1]];

    if(selected)
    {
        
        [UIView animateWithDuration:1.f animations:^{
            self.percentanswer.value = 100.f;
        }];
  
    }
    else
    {
               [UIView animateWithDuration:1.f animations:^{
            self.percentanswer.value = 0.f;
        }];
    }
    

    // Configure the view for the selected state
}
-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
   }
@end

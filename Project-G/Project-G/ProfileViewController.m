//
//  ProfileViewController.m
//  Project-G
//
//  Created by Sabari on 17/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ProfileViewController.h"
#import "Urlclass.h"

@interface ProfileViewController ()
{
    NSDictionary *dictResponse, *dictResponse1,*dictresponse3;
    NSString *ipaddress;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    ipaddress=@"aktrea.com/fintech";
    [self getaprofile];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief get profile details using groupid
 */
-(void) getaprofile
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetProfileDetails/%@",ipaddress,_groupid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               _name.text=[dictResponse valueForKey:@"UserName"];
                                                                               _email.text=[dictResponse valueForKey:@"Email"];
                                                                               _company.text=[dictResponse valueForKey:@"CompanyName"];
                                                                           });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}

@end

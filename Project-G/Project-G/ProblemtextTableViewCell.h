//
//  ProblemtextTableViewCell.h
//  Project-G
//
//  Created by Sabari on 17/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProblemtextTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *porblemtext;
@property (strong, nonatomic) IBOutlet UIButton *solutionbut;
@property (strong, nonatomic) IBOutlet UIButton *likebut;
@property (strong, nonatomic) IBOutlet UILabel *likecount;

@end

//
//  ComponentViewController.m
//  Project-G
//
//  Created by Sabari on 16/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "ComponentViewController.h"
#import "EventViewController.h"
#import "Urlclass.h"
#import "GetdataApi.h"
@interface ComponentViewController ()
{
    NSArray *data1,*event,*imagename;
    NSString *ipaddress,*groupid;
    NSDictionary *dictResponse,*dictresponse3;
}

@end

@implementation ComponentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    [self getuserevent];
    data1=@[@"Agenda",@"Quick Polls",@"Group Discussion",@"Learning Game"];
    event=@[@"Pre placement talk - Rajiv Rajendra, CEO Aktrea",@"Results announcement and thank you note - Ajay Ramachandran, CTO Aktrea"];
    imagename=@[@"Checklist-96.png",@"Poll Topic-96.png",@"User Groups Filled-100.png",@"Courses Filled-100.png"];
    self.navigationItem.title=@"Components";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) getuserevent
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/agendaevent/getuserevents",ipaddress];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               [_componenttable reloadData];
                                                                           });
                                                            
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
    {
        return @"";
    }
    else if(section==1)
    {
        return @"My Events";
    }
    else if(section==2)
    {
        return @"Live Events";
    }
    else
    {
        return @"Next Events";
    }
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return 4;
    }
    else if(section==1)
    {
        return [dictResponse count];
    }
    else if(section==2)
    {
        return 0;
    }
    else
    {
        return 0;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    UILabel *value=(UILabel *)[cell viewWithTag:1];
    UIImageView *img=(UIImageView *)[cell viewWithTag:2];
    if(indexPath.section==0)
    {
        value.text=[data1 objectAtIndex:indexPath.row];
        img.image=[UIImage imageNamed:[imagename objectAtIndex:indexPath.row]];
    }
    else if(indexPath.section==1)
    {
        value.text=[[dictResponse valueForKey:@"Topic" ] objectAtIndex:indexPath.row];
        img.image=[UIImage imageNamed:@"Event Accepted-96.png"];
    }
    else if(indexPath.section==2)
    {
        value.text=@"";
    }
    else
    {
        value.text=@"";
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0)
    {
        if(indexPath.row == 0)
        {
            [self performSegueWithIdentifier:@"agenda" sender:[data1 objectAtIndex:indexPath.row]];
        }
        else if (indexPath.row==1)
        {
            [self performSegueWithIdentifier:@"polls" sender:[data1 objectAtIndex:indexPath.row]];
        }
        else if (indexPath.row==2)
        {
            [self performSegueWithIdentifier:@"group" sender:[data1 objectAtIndex:indexPath.row]];
        }
    }
    else if(indexPath.section==1)
    {
        [self performSegueWithIdentifier:@"event" sender:[[dictResponse valueForKey:@"GroupId"]objectAtIndex:indexPath.row]];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString: @"event"])
    {
        EventViewController *evt=[segue destinationViewController];
        evt.groupid=[sender copy];
    }
    
    else if([segue.identifier isEqualToString: @"agenda"])
    {
        [segue destinationViewController];
    }
    else if([segue.identifier isEqualToString: @"polls"])
    {
        [segue destinationViewController];
    }
    else if([segue.identifier isEqualToString: @"group"])
    {
        [segue destinationViewController];
    }
    
}
@end

//
//  StatusEventViewController.m
//  Project-G
//
//  Created by Sabari on 1/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "StatusEventViewController.h"
#import "SCLAlertView.h"
#import "Urlclass.h"
#import "ASIFormDataRequest.h"
#import "GetdataApi.h"
@interface StatusEventViewController ()
{
    NSMutableDictionary *dictResponse, *dictResponse1,*dictresponse3;
    NSString *ipaddress,*statusvalue;
    int posttype;
    NSData *imageData;
    
}
@end

@implementation StatusEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    _statustextmessage.textColor = [UIColor grayColor];
    _statustextmessage.delegate=self;
    _imagedectext.delegate=self;
    posttype=0;
    _username.text=[[NSUserDefaults standardUserDefaults]stringForKey:@"currentusername"];
    _imageview.hidden=YES;
    [[_Publish_but layer] setBorderWidth:1.0f];
    [[_Publish_but layer] setBorderColor:[UIColor colorWithRed:3.0f/255 green:179.0f/255 blue:247.0f/255 alpha:1].CGColor];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief handle select photo or camera for iphnoe,ipad(popoverPresentationController).
 */
- (IBAction)addphoto:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped do nothing.
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // take photo button tapped.
        [self takePhoto];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // choose photo button tapped.
        [self choosePhoto];
        
    }]];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        [actionSheet.popoverPresentationController setPermittedArrowDirections:0];
        
        //For set action sheet to middle of view.
        actionSheet.popoverPresentationController.sourceView = self.view;
        actionSheet.popoverPresentationController.sourceRect = self.view.bounds;
        
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
    else
    {
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}
/*!
 *@brief if user select camera it open camera(UIImagePickerControllerSourceTypeCamera)
 */
-(void)takePhoto
{
    UIImagePickerController *controller = [[UIImagePickerController alloc] init];
    controller.sourceType = UIImagePickerControllerSourceTypeCamera;
    controller.allowsEditing = NO;
    controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
    controller.delegate = self;
    [self.navigationController presentViewController: controller animated: YES completion: nil];
}
/*!
 *@brief if user select album it open album(UIImagePickerControllerSourceTypePhotoLibrary)
 */
-(void)choosePhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
/*!
 *@brief after select image get photo and convert to NSData assign to image data.
 */
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * myImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    _imageview.hidden=NO;
    _imagepost.image=myImage;
    imageData = UIImagePNGRepresentation(myImage);
    if(imageData != nil)
    {
        posttype=2;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    //[info objectForKey:UIImagePickerControllerOriginalImage];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)Publish_but:(id)sender {
    _Publish_but.userInteractionEnabled=NO;
    statusvalue=_statustextmessage.text;
    if(posttype==0)
    {
        [self poststatus];
    }
    else if(posttype==1)
    {
        [self postproblem];
    }
    else if(posttype==2)
    {
        [self postimage];
    }
}
/*!
 *@brief post status, post solutiondata(NSDictionary) in post method and update solution data based on responce.
 */
-(void)poststatus
{
    if([_statustextmessage.text isEqualToString:@""]||[_statustextmessage.text isEqualToString:@"What's on your mind?"])
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert showError:self title:@"Oops"
                subTitle:@"Please type your Status" closeButtonTitle:@"OK" duration:0.0f];
        
    }
    else
    {
        _statustextmessage.text=@"";
        NSDictionary *solutiondata1 = [NSDictionary dictionaryWithObjectsAndKeys:statusvalue,@"PostContent",_groupid,@"GroupId",nil];
        NSDictionary *solutiondata=[NSDictionary dictionaryWithObjectsAndKeys:solutiondata1,@"Posts",nil];
        NSLog(@"%@",solutiondata);
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSError *ss;
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/Post",ipaddress]];
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:solutiondata options:NSJSONWritingPrettyPrinted error:&ss];
        
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        
        [urlRequest setHTTPBody:jsonData];
        NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if(error == nil)
                                              {
                                                  
                                                  dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                  NSLog(@"Responce for server%@",dictresponse3);
                                                  [self.navigationController popViewControllerAnimated:YES];
                                                  _Publish_but.userInteractionEnabled=YES;
                                                  
                                              }
                                              else{
                                                  NSLog(@"Error %@",[error localizedDescription]);
                                              }
                                              
                                          }];
        
        [dataTask resume];
        
        
        
    }
    
}
/*!
 *@brief post problem, postdata(NSDictionary) in post method and update problem data based on responce.
 */
-(void)postproblem
{
    if([_statustextmessage.text isEqualToString:@"Post the problem"]||[_statustextmessage.text isEqualToString:@""])
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert showError:self title:@"Oops"
                subTitle:@"Please type your problem" closeButtonTitle:@"OK" duration:0.0f];
        
    }
    else
    {
        _statustextmessage.text=@"";
        NSDictionary *postdata = [NSDictionary dictionaryWithObjectsAndKeys:_groupid,@"GroupId",statusvalue,@"PostContent",@"",@"GeographicId",nil];
        NSLog(@"%@",postdata);
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSError *ss;
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/api/Problem/PostProblemForGroup",ipaddress]];
        
        NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:url];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postdata options:NSJSONWritingPrettyPrinted error:&ss];
        
        [urlRequest setHTTPMethod:@"POST"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [urlRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]] forHTTPHeaderField:@"Content-Length"];
        
        [urlRequest setHTTPBody:jsonData];
        NSURLSessionDataTask * dataTask =[defaultSession dataTaskWithRequest:urlRequest
                                                           completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              if(error == nil)
                                              {
                                                  NSString * text = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
                                                  NSLog(@"Responce for server%@",text);
                                                  [self.navigationController popViewControllerAnimated:YES];
                                                  _Publish_but.userInteractionEnabled=YES;}
                                              else{
                                                  NSLog(@"Error %@",[error localizedDescription]);
                                              }
                                              
                                          }];
        
        [dataTask resume];
        
        
        
    }
    
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView

{
    textView.autocorrectionType = UITextAutocorrectionTypeNo;
    
    if([_statustextmessage.text isEqualToString:@"What's on your mind?"]||[_statustextmessage.text isEqualToString:@"Post the problem"]||[_imagedectext.text isEqualToString:@"Description"])
    {
        _statustextmessage.text = @"";
    }
    _statustextmessage.textColor = [UIColor blackColor];
    return YES;
}
- (BOOL) textView: (UITextView*) textView
shouldChangeTextInRange: (NSRange) range
  replacementText: (NSString*) text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    else if([_statustextmessage.text isEqualToString:@"What's on your mind?"]||[_statustextmessage.text isEqualToString:@"Post the problem"])
    {
        _statustextmessage.text = @"";
        _imagedectext.text=@"";
    }
    _imagedectext.textColor = [UIColor blackColor];
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    if(textView == _statustextmessage)
    {
        [self.statustextmessage endEditing:YES];
    }
    return YES;
}

/*!
 *@brief upadate type of post based on user selection
 */
- (IBAction)status:(id)sender {
    _statustextmessage.text=@"What's on your mind?";
    posttype=0;
}
/*!
 *@brief upadate type of post based on user selection
 */
- (IBAction)problem:(id)sender {
    _statustextmessage.text=@"Post the problem";
    posttype=1;
}
/*!
 *@brief post image by post image data and key as Document0
 */
-(void)postimage
{
    NSString *imgdec;
    if([_imagedectext.text isEqualToString:@"Description"])
    {
        imgdec=@"";
    }
    else
    {
        imgdec=[NSString stringWithFormat:@"%@",_imagedectext.text];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api/file/UploadDocument?groupId=%@&documentText=%@",ipaddress,_groupid,imgdec];
    NSLog(@"%@",imgdec);
    NSURL *requestUrl = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:
                                              NSASCIIStringEncoding]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:requestUrl]; // Upload a file on disk
    [request setData:imageData withFileName:@"iosimage.png" andContentType:@"image/png" forKey:@"Document0"];
    [request setRequestMethod:@"POST"];
    //[request appendPostData:body];
    [request setDelegate:self];
    [request setTimeOutSeconds:10.0];
    request.shouldAttemptPersistentConnection = NO;
    [request setDidFinishSelector:@selector(uploadRequestFinished:)];
    [request setDidFailSelector:@selector(uploadRequestFailed:)];
    [request startAsynchronous];
    
    
    
}
/*!
 *@brief get responce from server wheather image upload or not. If imahge uploaded it will update in post.
 */
- (void)uploadRequestFinished:(ASIHTTPRequest *)request{
    [self.navigationController popViewControllerAnimated:YES];
    _Publish_but.userInteractionEnabled=YES;
}

- (void)uploadRequestFailed:(ASIHTTPRequest *)request{
    
    NSLog(@" Error - Statistics file upload failed: \"%@\"",[[request error] localizedDescription]);
    _Publish_but.userInteractionEnabled=YES;}

@end

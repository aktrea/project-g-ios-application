//
//  UserlistViewController.m
//  Project-G
//
//  Created by Sabari on 9/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "UserlistViewController.h"
#import "ChatViewController.h"
@interface UserlistViewController ()
{
    NSString *ipaddress;
    int userid,selectrow;
    NSInteger row,sections;
}

@end

@implementation UserlistViewController
@synthesize memberlist,uservalue,userlistTable,groupid,groupname;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *userid1=[[NSUserDefaults standardUserDefaults]stringForKey:@"userid"];
    userid=[userid1 intValue];
    
    NSLog(@"%d",userid);
    _val=[[NSMutableArray alloc]init];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       memberlist=[[NSUserDefaults standardUserDefaults]valueForKey:@"memberlist"];
                       uservalue=[[NSUserDefaults standardUserDefaults]valueForKey:@"userconid"];
                       groupname=[[NSUserDefaults standardUserDefaults]valueForKey:@"onlinegroupname"];
                       groupid=[[NSUserDefaults standardUserDefaults]valueForKey:@"onlinegroupid"];
                       userlistTable.backgroundColor=[UIColor clearColor];
                       [userlistTable reloadData];
                   });
    [_val addObject:[NSNumber numberWithInt:userid]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(yourRefreshMethodHere:)
                                                 name:@"updateLeftTable"
                                               object:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)prefersStatusBarHidden{
    return YES;
}
/*!
 *@brief display online user and group
 */
-(void)viewWillAppear:(BOOL)animated
{
    
    userlistTable.delegate=self;
    userlistTable.dataSource=self;
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       memberlist=[[NSUserDefaults standardUserDefaults]valueForKey:@"memberlist"];
                       uservalue=[[NSUserDefaults standardUserDefaults]valueForKey:@"userconid"];
                       _touserid=[[NSUserDefaults standardUserDefaults]valueForKey:@"touserid"];
                       groupname=[[NSUserDefaults standardUserDefaults]valueForKey:@"onlinegroupname"];
                       groupid=[[NSUserDefaults standardUserDefaults]valueForKey:@"onlinegroupid"];
                       
                       [userlistTable reloadData];
                   });
    
}

- (void)yourRefreshMethodHere:(NSNotification *)notif {
    if ([[notif name] isEqualToString:@"updateLeftTable"])
    {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           userlistTable.delegate=self;
                           userlistTable.dataSource=self;
                           memberlist=[[NSUserDefaults standardUserDefaults]valueForKey:@"memberlist"];
                           uservalue=[[NSUserDefaults standardUserDefaults]valueForKey:@"userconid"];
                           _touserid=[[NSUserDefaults standardUserDefaults]valueForKey:@"touserid"];
                           groupname=[[NSUserDefaults standardUserDefaults]valueForKey:@"onlinegroupname"];
                           groupid=[[NSUserDefaults standardUserDefaults]valueForKey:@"onlinegroupid"];
                           
                           
                           [userlistTable reloadData];
                           // [self viewDidLoad];
                       });
        
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==1)
    {
        return [memberlist count];
    }
    else
    {
        return [groupname count];
    }
}
/*!
 *@brief display username and group name
 */
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.textColor=[UIColor blackColor];
    cell.detailTextLabel.textColor=[UIColor blackColor];
    if(indexPath.section==1)
    {
        
        cell.textLabel.text=[memberlist objectAtIndex:indexPath.row];
        cell.imageView.image=[UIImage imageNamed:@"profiledefault.jpg"];
        cell.detailTextLabel.text=@"Member";
        
        NSLog(@"%@",[memberlist objectAtIndex:indexPath.row]);
        return cell;
    }
    else
    {
        cell.textLabel.text=[groupname objectAtIndex:indexPath.row];
        cell.imageView.image=[UIImage imageNamed:@"group.png"];
        cell.detailTextLabel.text=@"Group";
        return cell;
    }
}
/*!
 *@brief user can select and chat. redirect to chat page
 */
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    row=indexPath.row;
    sections=indexPath.section;
    
    
    [self performSegueWithIdentifier:@"messageview" sender:self];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}
/*!
 *@brief redirect to ChatViewController
 */
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    ChatViewController *cht=[segue destinationViewController];
    if(sections==1)
        
    {
        NSLog(@"%@",[[uservalue objectAtIndex:row] copy]);
        NSLog(@"%@",[[memberlist objectAtIndex:row] copy]);
        NSLog(@"%@",[[_touserid objectAtIndex:row] copy]);
        cht.getstatus=true;
        cht.uservalue=[[uservalue objectAtIndex:row] copy];
        cht.frmvalue=[[memberlist objectAtIndex:row]copy];
        cht.touserid=[[_touserid objectAtIndex:row]copy];
    }
    else if(sections==0)
    {
        cht.getstatus=false;
        cht.groupname=[[groupname objectAtIndex:row]copy];
        cht.groupid=[[groupid objectAtIndex:row]copy];
    }
    
}

@end

//
//  SearchViewController.h
//  Project-G
//
//  Created by Sabari on 17/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UISearchBarDelegate, UISearchDisplayDelegate,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (strong, nonatomic) IBOutlet UITableView *searchtable;
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText; 
@end

//
//  ProfileViewController.h
//  Project-G
//
//  Created by Sabari on 17/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *profilecover;
@property (strong, nonatomic) IBOutlet UIImageView *profileimg;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *company;
@property NSString *groupid;
@end

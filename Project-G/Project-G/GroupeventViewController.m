//
//  GroupeventViewController.m
//  Project-G
//
//  Created by Sabari on 7/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import "GroupeventViewController.h"
#import "CHCircularCollectionLayout.h"
#import "UIColor+Custom.h"
#import "UIView+Custom.h"
#import "GroupdiscussionViewController.h"
#import "Urlclass.h"
static NSString *kCollectionViewCell = @"CollectionViewCellIdentifier";
@interface GroupeventViewController ()
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;


@end

@implementation GroupeventViewController
{
    NSArray *items;
    NSMutableArray *colors;
    NSString *ipaddress,*groupid;
    NSDictionary *dictResponse,*dictresponse3;
    NSArray *searchResults,*dictResponse1,*search;
    UIImageView *img;
    NSString *status;
    bool tablestatus;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    groupid=[[NSUserDefaults standardUserDefaults]stringForKey:@"groupid"];
    Urlclass *sharedManager = [Urlclass sharedManager];
    ipaddress=sharedManager.urlstring;
    self.navigationItem.title=@"Group Discussion";
    _collectionView.backgroundColor = [UIColor clearColor];
    tablestatus=false;
    [self gettopic];
    [self getparticipants];
    [self getstatus];
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*!
 *@brief get participant list based on groupid. get participant list in dictResponse1 and load the data in CollectionView to display.
 */
-(void) getparticipants
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/social/GetParticipantList/%@",ipaddress,groupid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse1=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               tablestatus=true;
                                                                               [_collectionView reloadData];
                                                                           });
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}
/*!
 *@brief get group discussion topic list from server using NSURLSession and display details.
 */
-(void) gettopic
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/agendaevent/GetTopicDetailsById?groupId=%@",ipaddress,groupid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictResponse=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            dispatch_async(dispatch_get_main_queue(),
                                                                           ^{
                                                                               
                                                                               _topicname.text=[dictResponse valueForKey:@"TopicName"];
                                                                               _date.text=[dictResponse valueForKey:@"DateTime"];
                                                                           });
                                                            
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // return [[(NSArray *)items objectAtIndex:section] count];
    if(tablestatus==false)
    {
        return 10;
    }
    else
    {
        return [dictResponse1 count];
    }
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCell forIndexPath:indexPath];
    NSLog(@"AAA%ld",(long)indexPath.row);
    // give each some colour
    
    cell.backgroundColor = [UIColor clearColor];
    img=(UIImageView *)[cell viewWithTag:1];
    img.layer.cornerRadius=img.frame.size.height/2;
    img.layer.masksToBounds=YES;
    img.image=[UIImage imageNamed:@"Screen Shot 2016-11-01 at 5.01.02 PM.png"];
    [img.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [img.layer setBorderWidth: 1.0];
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(tablestatus==true)
    {
        NSArray *value = [dictResponse1 objectAtIndex:indexPath.row];
        _username.text=[NSString stringWithFormat:@"%@",[value valueForKey:@"UserName"]];
        UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath:indexPath];
        UIImageView *img1 = (UIImageView *)[cell viewWithTag:1];
        img1.layer.borderWidth = 1.0;
        img1.layer.borderColor = [UIColor redColor].CGColor;
    }
}
/*!
 *@brief Display participant name based on user selection.
 */
-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath:indexPath];
    UIImageView *img1 = (UIImageView *)[cell viewWithTag:1];
    img1.layer.borderWidth = 1.0;
    img1.layer.borderColor = [UIColor blackColor].CGColor;
}
/*!
 *@brief jointevnt ,leave event based on user status.
 */
- (IBAction)jointable:(id)sender {
    if([[_jointable titleForState:UIControlStateNormal] isEqualToString:@"Join Table"])
    {
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
        NSString *urlstr=[NSString stringWithFormat:@"%@/api/event/Jointable?groupId=%@",ipaddress,groupid];
        
        NSURL * url = [NSURL URLWithString:urlstr];
        
        NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if(error == nil)
                                                            {
                                                                
                                                                dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                                status=[NSString stringWithFormat:@"%@",dictresponse3];
                                                                
                                                                if([status isEqualToString:@"Joined"])
                                                                {
                                                                    
                                                                    [_jointable setTitle:@"Leave Table" forState:UIControlStateNormal];
                                                                    [self getstatus];
                                                                }
                                                                else
                                                                {
                                                                    [_jointable setTitle:status forState:UIControlStateNormal];
                                                                    [self getstatus];
                                                                }
                                                            }
                                                            
                                                        }];
        
        [dataTask resume];
    }
    else if([[_jointable titleForState:UIControlStateNormal] isEqualToString:@"Leave Table"])
    {
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
        NSString *urlstr=[NSString stringWithFormat:@"%@/api/event/LeaveTable?groupId=%@",ipaddress,groupid];
        
        NSURL * url = [NSURL URLWithString:urlstr];
        
        NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if(error == nil)
                                                            {
                                                                
                                                                dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                                status=[NSString stringWithFormat:@"%@",dictresponse3];
                                                                
                                                                if([status isEqualToString:@"Initial"])
                                                                {
                                                                    
                                                                    [_jointable setTitle:@"Join Table" forState:UIControlStateNormal];
                                                                    [self getstatus];
                                                                }
                                                                else  if([status isEqualToString:@"Initial"])
                                                                {
                                                                    [_jointable setTitle:@"Join Table" forState:UIControlStateNormal];
                                                                    [self getstatus];
                                                                }
                                                                else
                                                                {
                                                                    [_jointable setTitle:status forState:UIControlStateNormal];
                                                                    [self getstatus];
                                                                }
                                                            }
                                                            
                                                        }];
        
        [dataTask resume];
    }
    
    
}
/*!
 *@brief get status from server using NSURLSession.if user joined post will be display call([self geteventpost:0 :scrollvalue]). if not post will be hide.
 */
-(void)getstatus
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: self delegateQueue: [NSOperationQueue mainQueue]];
    NSString *urlstr=[NSString stringWithFormat:@"%@/api/event/status?groupId=%@",ipaddress,groupid];
    
    NSURL * url = [NSURL URLWithString:urlstr];
    
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithURL:url
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if(error == nil)
                                                        {
                                                            
                                                            dictresponse3=[NSJSONSerialization JSONObjectWithData:data  options:-1 error:NULL];
                                                            status=[NSString stringWithFormat:@"%@",dictresponse3];
                                                            if([status isEqualToString:@"Joined"])
                                                            {
                                                                
                                                                [_jointable setTitle:@"Leave Table" forState:UIControlStateNormal];
                                                            }
                                                            else  if([status isEqualToString:@"Initial"])
                                                            {
                                                                [_jointable setTitle:@"Join Table" forState:UIControlStateNormal];
                                                                
                                                            }
                                                            else
                                                            {
                                                                [_jointable setTitle:status forState:UIControlStateNormal];
                                                            }
                                                        }
                                                        
                                                    }];
    
    [dataTask resume];
    
}

@end

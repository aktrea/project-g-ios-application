//
//  AgendaViewController.h
//  Project-G
//
//  Created by Sabari on 25/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *agendatable;

@end

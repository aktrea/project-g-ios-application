//
//  GroupParticipantViewController.h
//  Project-G
//
//  Created by Sabari on 17/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupParticipantViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property (strong, nonatomic) IBOutlet UITableView *parttable;
- (IBAction)participant_type:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *participant_type;
@property NSString *groupid;
@property (strong, nonatomic) IBOutlet UITableView *search_bar;

@end

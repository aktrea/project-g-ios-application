//
//  StatusViewController.h
//  Project-G
//
//  Created by Sabari on 21/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatusViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,NSURLSessionDelegate>
- (IBAction)addphoto:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *statustextmessage;
@property (strong, nonatomic) IBOutlet UILabel *username;
@property (strong, nonatomic) IBOutlet UIImageView *userimage;
@property (strong, nonatomic) IBOutlet UIButton *Publish_but;
- (IBAction)Publish_but:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *imageview;
@property (strong, nonatomic) IBOutlet UITextView *imagedectext;
@property (strong, nonatomic) IBOutlet UIImageView *imagepost;

@end

//
//  ChatViewController.h
//  Enthiran
//
//  Created by Sabari on 21/9/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignalR.h"

@interface ChatViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
{
    
}
//@property(nonatomic,assign) DesisionViewController *dec;
@property (weak, nonatomic) IBOutlet UITableView *chattable;
- (IBAction)send:(id)sender;
@property (strong ,nonatomic) NSString *uservalue;
@property (strong ,nonatomic) NSString *frmvalue;
@property (strong ,nonatomic) NSString *touserid;
@property (strong ,nonatomic) NSString *touseridfrommsg;
@property (strong ,nonatomic) NSString *groupid;
@property (strong ,nonatomic) NSString *groupname;
@property bool getstatus;
@property NSMutableArray *val1,*val,*val2,*chatmessage,*groupchatmessage;
- (void)sendPrivateMessage:(id)connectionid fromUsername:(NSString *)fromusername  Message:(NSString *)message Tousername:(NSString *)tousername DateHelper:(NSString *)date FrmName:(NSString *)frm_user Toid:(id)to_id;
-(void)GroupMessage:(NSString *)uname Message:(NSString *)groupMessage groupid:(NSString*)Groupid Gname:(NSString *)gname DateHelper:(NSString *)date;

@end

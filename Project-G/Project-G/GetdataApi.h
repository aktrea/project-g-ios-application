//
//  GetdataApi.h
//  Project-G
//
//  Created by Sabari on 3/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetdataApi : NSObject

+ (NSURLSession *)dataSession;
+ (void)fetchContentsOfURL:(NSURL *)url
                completion:(void (^)(NSData *data, NSError *error)) completionHandler;
+(void)postContentsOfURL:(NSURL *)url :(NSDictionary *)dict completion:(void (^)(NSData *data, NSError *error)) completionHandler;
@end

//
//  SettingsViewController.m
//  Project-G
//
//  Created by Sabari on 9/1/17.
//  Copyright © 2017 aktrea. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifier];
    }
    cell.textLabel.text=@"Logout";
    return cell;
}
/*!
 *@brief handle logout if select logout alert will shown for logout.
 */


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Logout"
                                  message:@"Are you sure want to logout?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okbutton = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   UIViewController *loginView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                   [self presentViewController:loginView animated:YES completion:nil];
                                   
                               }];
    UIAlertAction* cancelbutton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action)
                                   {
                                   }];
    [alert addAction:okbutton];
    [alert addAction:cancelbutton];
    [self presentViewController:alert animated:YES completion:nil];
}
@end

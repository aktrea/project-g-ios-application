//
//  EventViewController.h
//  Project-G
//
//  Created by Sabari on 30/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UIImageView *coverimage;
- (IBAction)editcoverimage:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *eventnametext;
@property (strong, nonatomic) IBOutlet UILabel *eventdatetext;
@property (strong, nonatomic) IBOutlet UILabel *eventspeakertext;
@property (strong, nonatomic) IBOutlet UITableView *eventtable;
@property (strong, nonatomic) IBOutlet UITextView *statustext;
@property (strong, nonatomic) IBOutlet UIButton *joinevent;
- (IBAction)joinevent:(id)sender;
- (IBAction)Navigatebut:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *eventview;
- (IBAction)participantsbut:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *statusview;

@property NSString *groupid;
@end

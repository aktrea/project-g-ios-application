//
//  Urlclass.m
//  Pods
//
//  Created by Sabari on 3/1/17.
//
//

#import "Urlclass.h"

@implementation Urlclass
@synthesize urlstring;

#pragma mark Singleton Methods

+ (id)sharedManager {
    static Urlclass *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
/*!
 *@brief assign url to urlstring to access from all ViewController 
 */
- (id)init {
    if (self = [super init]) {
       // urlstring = @"http://192.168.3.120/akton";
        urlstring = @"https://aktrea.com/fintech";
    }
    return self;
}


@end

//
//  CommentTableViewCell.h
//  Project-G
//
//  Created by Sabari on 24/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *postedbyname;
@property (strong, nonatomic) IBOutlet UIImageView *posteduserimage;
@property (strong, nonatomic) IBOutlet UILabel *commenttext;
@property (strong, nonatomic) IBOutlet UILabel *posttime;

@end

//
//  SenderTableViewCell.h
//  Enthiran
//
//  Created by Sabari on 21/9/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SenderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *sendermessage;
@property (weak, nonatomic) IBOutlet UIImageView *senderimage;
@property (strong, nonatomic) IBOutlet UILabel *sendertime;
@property (strong, nonatomic) IBOutlet UILabel *senderpostedby;

@end

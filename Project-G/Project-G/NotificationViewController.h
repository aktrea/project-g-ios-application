//
//  NotificationViewController.h
//  Project-G
//
//  Created by Sabari on 16/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController<NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate,UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *notificationtable;


@end

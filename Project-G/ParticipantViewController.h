//
//  ParticipantViewController.h
//  Project-G
//
//  Created by Sabari on 1/12/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParticipantViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLSessionDelegate,NSURLSessionDataDelegate,NSURLSessionTaskDelegate>
@property (strong, nonatomic) IBOutlet UITableView *parttable;
- (IBAction)participant_type:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *participant_type;
@property NSString *groupid;
@end

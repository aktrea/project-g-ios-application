//
//  SolutionanswerTableViewCell.h
//  Project-G
//
//  Created by Sabari on 18/11/16.
//  Copyright © 2016 aktrea. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SolutionanswerTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *postedby;
@property (strong, nonatomic) IBOutlet UILabel *postedsolution;
@property (strong, nonatomic) IBOutlet UIImageView *postedimage;
@property (strong, nonatomic) IBOutlet UILabel *postedtime;

@end
